ssr_Header:
	smpsHeaderStartSong 1
	smpsHeaderVoice     ssr_Voices
	smpsHeaderChan      $06, $03
	smpsHeaderTempo     $02, $05

	smpsHeaderDAC       ssr_DAC
	smpsHeaderFM        ssr_FM1,	$00, $10
	smpsHeaderFM        ssr_FM2,	$00, $10
	smpsHeaderFM        ssr_FM3,	$00, $10
	smpsHeaderFM        ssr_FM4,	$00, $10
	smpsHeaderFM        ssr_FM5,	$00, $10
	smpsHeaderPSG       ssr_PSG1,	$DC, $02, $00, $00
	smpsHeaderPSG       ssr_PSG2,	$DC, $02, $00, $00
	smpsHeaderPSG       ssr_PSG3,	$DC, $02, $00, $00

; FM1 Data
ssr_FM1:
	smpsCall            ssr_Call25
	smpsCall            ssr_Call26
	smpsCall            ssr_Call27
	smpsCall            ssr_Call28
	smpsCall            ssr_Call29
	smpsCall            ssr_Call28
	smpsCall            ssr_Call2A
	smpsCall            ssr_Call2B
	smpsCall            ssr_Call2B
	smpsCall            ssr_Call2B
	smpsCall            ssr_Call2C
	smpsCall            ssr_Call2D
	smpsJump            ssr_FM1

; FM2 Data
ssr_FM2:
	smpsCall            ssr_Call1C
	smpsCall            ssr_Call1D
	smpsCall            ssr_Call1E
	smpsCall            ssr_Call1F
	smpsCall            ssr_Call20
	smpsCall            ssr_Call1F
	smpsCall            ssr_Call21
	smpsCall            ssr_Call22
	smpsCall            ssr_Call22
	smpsCall            ssr_Call22
	smpsCall            ssr_Call23
	smpsCall            ssr_Call24
	smpsJump            ssr_FM2

; FM3 Data
ssr_FM3:
	smpsCall            ssr_Call13
	smpsCall            ssr_Call14
	smpsCall            ssr_Call14
	smpsCall            ssr_Call14
	smpsCall            ssr_Call14
	smpsCall            ssr_Call15
	smpsCall            ssr_Call16
	smpsCall            ssr_Call17
	smpsCall            ssr_Call18
	smpsCall            ssr_Call19
	smpsCall            ssr_Call1A
	smpsCall            ssr_Call1B
	smpsJump            ssr_FM3

; FM4 Data
ssr_FM4:
	smpsCall            ssr_Call0C
	smpsCall            ssr_Call0D
	smpsCall            ssr_Call0D
	smpsCall            ssr_Call0D
	smpsCall            ssr_Call0E
	smpsCall            ssr_Call0F
	smpsCall            ssr_Call10
	smpsCall            ssr_Call10
	smpsCall            ssr_Call10
	smpsCall            ssr_Call10
	smpsCall            ssr_Call11
	smpsCall            ssr_Call12
	smpsJump            ssr_FM4

; FM5 Data
ssr_FM5:
	smpsCall            ssr_Call08
	smpsCall            ssr_Call08
	smpsCall            ssr_Call08
	smpsCall            ssr_Call08
	smpsCall            ssr_Call08
	smpsCall            ssr_Call08
	smpsCall            ssr_Call09
	smpsCall            ssr_Call0A
	smpsCall            ssr_Call0A
	smpsCall            ssr_Call0A
	smpsCall            ssr_Call0B
	smpsCall            ssr_Call08
	smpsJump            ssr_FM5

; PSG1 Data
ssr_PSG1:
	smpsCall            ssr_Call3E
	smpsCall            ssr_Call3F
	smpsCall            ssr_Call40
	smpsCall            ssr_Call41
	smpsCall            ssr_Call42
	smpsCall            ssr_Call41
	smpsCall            ssr_Call43
	smpsCall            ssr_Call44
	smpsCall            ssr_Call44
	smpsCall            ssr_Call44
	smpsCall            ssr_Call45
	smpsCall            ssr_Call3F
	smpsJump            ssr_PSG1

; PSG2 Data
ssr_PSG2:
	smpsCall            ssr_Call35
	smpsCall            ssr_Call36
	smpsCall            ssr_Call37
	smpsCall            ssr_Call38
	smpsCall            ssr_Call39
	smpsCall            ssr_Call38
	smpsCall            ssr_Call3A
	smpsCall            ssr_Call3B
	smpsCall            ssr_Call3B
	smpsCall            ssr_Call3B
	smpsCall            ssr_Call3C
	smpsCall            ssr_Call3D
	smpsJump            ssr_PSG2

; PSG3 Data
ssr_PSG3:
	smpsPSGform         $E7
	smpsCall            ssr_Call2E
	smpsCall            ssr_Call2F
	smpsCall            ssr_Call30
	smpsCall            ssr_Call31
	smpsCall            ssr_Call31
	smpsCall            ssr_Call32
	smpsCall            ssr_Call33
	smpsCall            ssr_Call31
	smpsCall            ssr_Call31
	smpsCall            ssr_Call31
	smpsCall            ssr_Call31
	smpsCall            ssr_Call34
	smpsJump            ssr_PSG3

; DAC Data
ssr_DAC:
	smpsCall            ssr_Call00
	smpsCall            ssr_Call01
	smpsCall            ssr_Call02
	smpsCall            ssr_Call03
	smpsCall            ssr_Call04
	smpsCall            ssr_Call05
	smpsCall            ssr_Call06
	smpsCall            ssr_Call03
	smpsCall            ssr_Call03
	smpsCall            ssr_Call03
	smpsCall            ssr_Call03
	smpsCall            ssr_Call07
	smpsJump            ssr_DAC

ssr_Call25:
	smpsSetvoice        $0A
	dc.b	nBb2, $01, nCs3, nEb3, nFs3, nAb3, nBb3, nCs4, nEb4, nFs4, nAb4, nBb4
	dc.b	nCs5, nEb5, nFs5, nAb5, nBb5, $04, nRst, $6D
	smpsReturn

ssr_Call26:
	dc.b	smpsNoAttack, nRst, $0F, nF3, $05, nRst, $03, nF3, $05, nRst, $03, nEb3
	dc.b	$05, nRst, $03, nEb3, $01, nRst, $03, nCs3, $04, nRst, $0C, nCs3
	dc.b	$04, nRst, nEb3, $01, nRst, $03, nEb3, $05, nRst, $03, nF3, $05
	dc.b	nRst, $03, nF3, $05, nRst, $03, nEb3, $05, nRst, $03, nEb3, $01
	dc.b	nRst, $03, nCs3, $04, nRst, $0C, nCs3, $04, nRst, $01
	smpsReturn

ssr_Call27:
	dc.b	smpsNoAttack, nRst, $03, nEb3, $01, nRst, $03, nEb3, $05, nRst, $03, nF3
	dc.b	$02, nRst, nF3, nRst, nF3, $06, $02, nRst, $04, nF3, $06, nRst
	dc.b	$02, nF3, $04, nRst, nF3, $02, nRst, nF3, $06, $02, nRst, $04
	dc.b	nF3, $06, nRst, $02, nF3, $04, nRst, nEb3, $02, nRst, nEb3, $06
	dc.b	$02, nRst, $04, nEb3, $06, nRst, $02, nEb3, $04, nRst, nCs3, $02
	dc.b	nRst, nCs3, $06, $02, nRst, $01
	smpsReturn

ssr_Call28:
	dc.b	smpsNoAttack, nRst, $03, nCs3, $06, nRst, $02, nCs3, $04, nRst, nF3, $02
	dc.b	nRst, nF3, $06, $02, nRst, $04, nF3, $06, nRst, $02, nF3, $04
	dc.b	nRst, nF3, $02, nRst, nF3, $06, $02, nRst, $04, nF3, $06, nRst
	dc.b	$02, nF3, $04, nRst, nFs3, $02, nRst, nFs3, $06, $02, nRst, $04
	dc.b	nFs3, $06, nRst, $02, nFs3, $04, nRst, nAb3, $02, nRst, nAb3, $06
	dc.b	$02, $01
	smpsReturn

ssr_Call29:
	dc.b	smpsNoAttack, $05, nRst, $02, nAb3, $06, nRst, $02, nF3, nRst, nF3, nRst
	dc.b	nF3, $06, $02, nRst, $04, nF3, $06, nRst, $02, nF3, $04, nRst
	dc.b	nF3, $02, nRst, nF3, $06, $02, nRst, $04, nF3, $06, nRst, $02
	dc.b	nF3, $04, nRst, nEb3, $02, nRst, nEb3, $06, $02, nRst, $04, nEb3
	dc.b	$06, nRst, $02, nEb3, $04, nRst, nCs3, $02, nRst, nCs3, $06, $02
	dc.b	nRst, $01
	smpsReturn

ssr_Call2A:
	dc.b	smpsNoAttack, nRst, $05, nRst, $02, nAb3, $06, nRst, $73
	smpsReturn

ssr_Call2B:
	dc.b	smpsNoAttack, nRst, $7F, smpsNoAttack, nRst, $01
	smpsReturn

ssr_Call2C:
	dc.b	smpsNoAttack, nRst, $13
	smpsSetvoice        $1B
	dc.b	nG3, $06, nRst, $02, nC3, $06, nRst, $02, nC3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nG3, $06, nRst, $02, nC3, $06, nRst, $02
	dc.b	nC3, $06, nRst, $02, nF3, $04, nC3, $06, nRst, $02, nC4, $04
	dc.b	nC3, $06, nRst, $02, nC3, $06, nRst, $02, nF3, $06, nRst, $02
	dc.b	nG3, $06, nRst, $02, nC3, $05
	smpsReturn

ssr_Call2D:
	dc.b	smpsNoAttack, $01, nRst, $02, nC3, $06, nRst, $02, nF3, $04, nC3, $06
	dc.b	nRst, $02, nC4, $04, nC3, $06, nRst, $02, nC3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nG3, $06, nRst, $02, nC3, $06, nRst, $02
	dc.b	nC3, $06, nRst, $02, nF3, $06, nRst, $02, nG3, $06, nRst, $02
	dc.b	nC3, $06, nRst, $02, nC3, $06, nRst, $02, nF3, $06, nRst, $0F
	smpsReturn

ssr_Call1C:
	smpsSetvoice        $0A
	dc.b	nBb1, $01, nCs2, nEb2, nFs2, nAb2, nBb2, nCs3, nEb3, nFs3, nAb3, nBb3
	dc.b	nCs4, nEb4, nFs4, nAb4, nBb4, $04, nRst, $6D
	smpsReturn

ssr_Call1D:
	dc.b	smpsNoAttack, nRst, $0F, nBb2, $05, nRst, $03, nBb2, $05, nRst, $03, nBb2
	dc.b	$05, nRst, $03, nBb2, $01, nRst, $03, nBb2, $04, nRst, nBb2, nRst
	dc.b	nBb2, nRst, nBb2, $01, nRst, $03, nBb2, $05, nRst, $03, nBb2, $05
	dc.b	nRst, $03, nBb2, $05, nRst, $03, nBb2, $05, nRst, $03, nBb2, $01
	dc.b	nRst, $03, nBb2, $04, nRst, nBb2, nRst, nBb2, nRst, $01
	smpsReturn

ssr_Call1E:
	dc.b	smpsNoAttack, nRst, $03, nBb2, $01, nRst, $03, nBb2, $05, nRst, $03, nBb2
	dc.b	$02, nRst, nBb2, nRst, nBb2, $06, $02, nRst, $04, nBb2, $06, nRst
	dc.b	$02, nBb2, $04, nRst, nBb2, $02, nRst, nBb2, $06, $02, nRst, $04
	dc.b	nBb2, $06, nRst, $02, nBb2, $04, nRst, nAb2, $02, nRst, nAb2, $06
	dc.b	$02, nRst, $04, nAb2, $06, nRst, $02, nAb2, $04, nRst, nFs2, $02
	dc.b	nRst, nFs2, $06, $02, nRst, $01
	smpsReturn

ssr_Call1F:
	dc.b	smpsNoAttack, nRst, $03, nFs2, $06, nRst, $02, nFs2, $04, nRst, nBb2, $02
	dc.b	nRst, nBb2, $06, $02, nRst, $04, nBb2, $06, nRst, $02, nBb2, $04
	dc.b	nRst, nBb2, $02, nRst, nBb2, $06, $02, nRst, $04, nBb2, $06, nRst
	dc.b	$02, nBb2, $04, nRst, nBb2, $02, nRst, nBb2, $06, $02, nRst, $04
	dc.b	nBb2, $06, nRst, $02, nBb2, $04, nRst, nC3, $02, nRst, nC3, $06
	dc.b	$02, $01
	smpsReturn

ssr_Call20:
	dc.b	smpsNoAttack, $05, nRst, $02, nC3, $06, nRst, $02, nBb2, nRst, nBb2, nRst
	dc.b	nBb2, $06, $02, nRst, $04, nBb2, $06, nRst, $02, nBb2, $04, nRst
	dc.b	nBb2, $02, nRst, nBb2, $06, $02, nRst, $04, nBb2, $06, nRst, $02
	dc.b	nBb2, $04, nRst, nAb2, $02, nRst, nAb2, $06, $02, nRst, $04, nAb2
	dc.b	$06, nRst, $02, nAb2, $04, nRst, nFs2, $02, nRst, nFs2, $06, $02
	dc.b	nRst, $01
	smpsReturn

ssr_Call21:
	dc.b	smpsNoAttack, nRst, $05, nRst, $02, nC3, $06, nRst, $73
	smpsReturn

ssr_Call22:
	dc.b	smpsNoAttack, nRst, $7F, smpsNoAttack, nRst, $01
	smpsReturn

ssr_Call23:
	dc.b	smpsNoAttack, nRst, $0F
	smpsSetvoice        $0A
	dc.b	nC3, $04, nG3, $02, nRst, nG3, $04, nC3, nBb3, nC3, nG3, nC3
	dc.b	nC3, nG3, $02, nRst, nG3, $04, nC3, nBb3, nC3, nG3, nC3, nC3
	dc.b	nF3, $02, nRst, nF3, $04, nC3, nBb3, nC3, nF3, nC3, nC3, nEb3
	dc.b	$02, nRst, nEb3, $04, nC3, nBb3, $01
	smpsReturn

ssr_Call24:
	dc.b	smpsNoAttack, $03, nC3, $04, nEb3, nC3, nC3, nG3, $02, nRst, nG3, $04
	dc.b	nC3, nBb3, nC3, nG3, nC3, nC3, nG3, $02, nRst, nG3, $04, nC3
	dc.b	nBb3, nC3, nG3, nC3, nC3, nAb3, $02, nRst, nAb3, $04, nC3, nBb3
	dc.b	nC3, nAb3, nC3, nC3, nRst, $0D
	smpsReturn

ssr_Call13:
	dc.b	smpsNoAttack, nRst, $0F
	smpsSetvoice        $1B
	dc.b	nBb2, $06, nRst, $02, nBb3, $06, nRst, $02, nF3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nF3, $06, nRst, $02, nBb2, $06, nRst, $02
	dc.b	nBb3, $06, nRst, $02, nF3, $06, nRst, $02, nF3, $06, nRst, $02
	dc.b	nBb2, $06, nRst, $02, nBb3, $06, nRst, $02, nF3, $01
	smpsReturn

ssr_Call14:
	dc.b	smpsNoAttack, $05, nRst, $02, nF3, $06, nRst, $02, nBb2, $06, nRst, $02
	dc.b	nBb3, $06, nRst, $02, nF3, $06, nRst, $02, nF3, $06, nRst, $02
	dc.b	nBb2, $06, nRst, $02, nBb3, $06, nRst, $02, nF3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nF3, $06, nRst, $02, nBb2, $06, nRst, $02
	dc.b	nBb3, $06, nRst, $02, nF3, $01
	smpsReturn

ssr_Call15:
	dc.b	smpsNoAttack, $05, nRst, $02, nF3, $06, nRst, $02, nBb2, $06, nRst, $02
	dc.b	nBb3, $06, nRst, $02, nF3, $06, nRst, $02, nF3, $06, nRst, $02
	dc.b	nBb2, $06, nRst, $02, nBb3, $06, nRst, $02, nF3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nF3, $06, nRst, $12, nC4, $01
	smpsReturn

ssr_Call16:
	dc.b	smpsNoAttack, $02, nRst, $01, nD4, $03, nRst, $01, nEb4, $02, nRst, nF4
	dc.b	nRst, nG4, $06, nRst, $02, nG4, $06, nRst, $02, nF4, $07, nRst
	dc.b	$01, nF4, $03, nRst, $01, nE4, $04, nRst, nC4, $08, nRst, $04
	dc.b	nC4, $03, nRst, $01, nD4, $03, nRst, $01, nEb4, $02, nRst, nF4
	dc.b	nRst, nG4, $06, nRst, $02, nG4, $06, nRst, $02, nF4, $07, nRst
	dc.b	$01, nF4, $03, nRst, $01, nG4, $13, nRst, $02
	smpsReturn

ssr_Call17:
	dc.b	smpsNoAttack, nRst, $03, nC4, $02, nRst, nEb4, $04, nRst, nG4, $06, nRst
	dc.b	$02, nG4, $06, nRst, $02, nF4, $07, nRst, $01, nF4, $03, nRst
	dc.b	$01, nE4, $04, nRst, nC4, $08, nRst, nEb4, $03, nRst, $01, nF4
	dc.b	$04, nRst, nG4, $06, nRst, nG4, $03, nRst, $01, nF4, $02, nRst
	dc.b	nF4, $06, nRst, $02, nG4, $13, nRst, $02
	smpsReturn

ssr_Call18:
	dc.b	smpsNoAttack, nRst, $03, nC4, $02, nRst, nEb4, $04, nRst, nG4, $06, nRst
	dc.b	$02, nG4, $06, nRst, $02, nF4, $07, nRst, $01, nF4, $03, nRst
	dc.b	$01, nE4, $04, nRst, nC4, $08, nRst, $04, nC4, $03, nRst, $01
	dc.b	nD4, $03, nRst, $01, nEb4, $02, nRst, nF4, nRst, nG4, $06, nRst
	dc.b	$02, nG4, $06, nRst, $02, nF4, $07, nRst, $01, nF4, $03, nRst
	dc.b	$01, nG4, $13, nRst, $01, nC4
	smpsReturn

ssr_Call19:
	dc.b	smpsNoAttack, $02, nRst, $01, nD4, $03, nRst, $01, nEb4, $02, nRst, nF4
	dc.b	nRst, nG4, $06, nRst, $02, nG4, $06, nRst, $02, nF4, $07, nRst
	dc.b	$01, nF4, $03, nRst, $01, nE4, $04, nRst, nC4, $08, nRst, nEb4
	dc.b	$03, nRst, $01, nF4, $04, nRst, nG4, $06, nRst, nG4, $03, nRst
	dc.b	$01, nF4, $02, nRst, nF4, $06, nRst, $02, nG4, $13, nRst, $02
	smpsReturn

ssr_Call1A:
	dc.b	smpsNoAttack, nRst, $0F, nC3, $06, nRst, $02, nC4, $04, $06, nRst, $02
	dc.b	nC4, nRst, nG3, $06, nRst, $02, nC3, $06, nRst, $02, nC4, $06
	dc.b	nRst, $02, nG3, $06, nRst, $02, nG3, $06, nRst, nG3, nRst, $02
	dc.b	nC4, $06, nRst, $02, nC4, nRst, nG3, $06, nRst, $02, nC3, $06
	dc.b	nRst, $02, nC4, $06, nRst, $02, nG3, $01
	smpsReturn

ssr_Call1B:
	dc.b	smpsNoAttack, $05, nRst, $02, nG3, $06, nRst, nG3, nRst, $02, nC4, $06
	dc.b	nRst, $02, nC4, nRst, nG3, $06, nRst, $02, nC3, $06, nRst, $02
	dc.b	nC4, $06, nRst, $02, nG3, $06, nRst, $02, nG3, $06, nRst, $02
	dc.b	nC3, $06, nRst, $02, nC4, $06, nRst, $02, nG3, $06, nRst, $02
	dc.b	nG3, $06, nRst, $02, nC3, $06, nRst, $0B
	smpsReturn

ssr_Call0C:
	dc.b	smpsNoAttack, nRst, $13
	smpsSetvoice        $1B
	dc.b	nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb2, $06, nRst, $02
	dc.b	nEb3, $06, nRst, $02, nF3, $06, nRst, $02, nBb2, $06, nRst, $02
	dc.b	nBb2, $06, nRst, $02, nEb3, $04, nRst, nF3, $06, nRst, $02, nBb2
	dc.b	$06, nRst, $02, nBb2, $06, nRst, $02, nEb3, $06, nRst, $02, nF3
	dc.b	$06, nRst, $02, nBb2, $05
	smpsReturn

ssr_Call0D:
	dc.b	smpsNoAttack, $01, nRst, $02, nBb2, $06, nRst, $02, nEb3, $04, nRst, nF3
	dc.b	$06, nRst, $02, nBb2, $06, nRst, $02, nBb2, $06, nRst, $02, nEb3
	dc.b	$06, nRst, $02, nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb2
	dc.b	$06, nRst, $02, nEb3, $04, nRst, nF3, $06, nRst, $02, nBb2, $06
	dc.b	nRst, $02, nBb2, $06, nRst, $02, nEb3, $06, nRst, $02, nF3, $06
	dc.b	nRst, $02, nBb2, $05
	smpsReturn

ssr_Call0E:
	dc.b	smpsNoAttack, $01, nRst, $02, nBb2, $06, nRst, $02, nEb3, $04, nRst, nF3
	dc.b	$06, nRst, $02, nBb2, $06, nRst, $02, nBb2, $06, nRst, $02, nEb3
	dc.b	$06, nRst, $02, nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb2
	dc.b	$06, nRst, $02, nEb3, $06, nRst, $02, nF3, $06, nRst, $02, nBb2
	dc.b	$06, nRst, $02, nBb2, $06, nRst, $02, nEb3, $04, nRst, nF3, $06
	dc.b	nRst, $02, nBb2, $05
	smpsReturn

ssr_Call0F:
	dc.b	smpsNoAttack, $01, nRst, $02, nBb2, $06, nRst, $02, nEb3, $06, nRst, $02
	dc.b	nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb2, $06, nRst, $02
	dc.b	nEb3, $04, nRst, nF3, $06, nRst, $02, nBb2, $06, nRst, $02, nBb2
	dc.b	$06, nRst, $02, nEb3, $06, nRst, $02, nF3, $06, nRst, $02, nBb2
	dc.b	$06, nRst, $02, nBb2, $06, nRst, $02, nEb3, $04, nRst, $11
	smpsReturn

ssr_Call10:
	dc.b	smpsNoAttack, nRst, $7F, smpsNoAttack, nRst, $01
	smpsReturn

ssr_Call11:
	dc.b	smpsNoAttack, nRst, $1F
	smpsSetvoice        $1B
	dc.b	nG3, $06, nRst, $02, nC4, nRst, nC4, nRst, $32, nG3, $06, nRst
	dc.b	$02, nC4, nRst, nC4, nRst, $13
	smpsReturn

ssr_Call12:
	dc.b	smpsNoAttack, nRst, $1F, nG3, $06, nRst, $02, nC4, nRst, nC4, nRst, $22
	dc.b	nBb4, $04, $02, nRst, nBb4, nRst, nBb4, $04, nRst, $21
	smpsReturn

ssr_Call08:
	dc.b	smpsNoAttack, nRst, $7F, smpsNoAttack, nRst, $01
	smpsReturn

ssr_Call09:
	dc.b	smpsNoAttack, nRst, $0F
	smpsSetvoice        $21
	dc.b	nEb5, nRst, $01, nD5, $0B, nRst, $01, nC5, $1A, nRst, $02, nD5
	dc.b	$08, nEb5, $0F, nRst, $01, nD5, $0B, nRst, $01, nE5, $15
	smpsReturn

ssr_Call0A:
	dc.b	smpsNoAttack, $05, nRst, $0A, nEb5, $0F, nRst, $01, nD5, $0B, nRst, $01
	dc.b	nC5, $1A, nRst, $02, nD5, $08, nEb5, $0F, nRst, $01, nD5, $0B
	dc.b	nRst, $01, nE5, $15
	smpsReturn

ssr_Call0B:
	dc.b	smpsNoAttack, $05, nRst, $7B
	smpsReturn

ssr_Call3E:
	smpsPSGvoice        fTone_06
	dc.b	nBb3, $01, nCs4, nEb4, nFs4, nAb4, nBb4, nCs5, nEb5, nFs5, nAb5, nBb5
	dc.b	nCs6, nEb6, nFs6, nAb6, nBb6, $04, nRst, $6D
	smpsReturn

ssr_Call3F:
	dc.b	smpsNoAttack, nRst, $7F, smpsNoAttack, nRst, $01
	smpsReturn

ssr_Call40:
	dc.b	smpsNoAttack, nRst, $0F
	smpsPSGvoice        fTone_06
	dc.b	nBb5, $01, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5
	dc.b	nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5
	dc.b	nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5
	dc.b	nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5
	dc.b	nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5
	dc.b	nRst, nBb5, nRst, nBb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5
	dc.b	nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5
	dc.b	nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5
	dc.b	nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5
	dc.b	nRst, nFs5, nRst, nFs5, nRst, nFs5
	smpsReturn

ssr_Call41:
	dc.b	nRst, $01, nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst
	dc.b	nFs5, nRst, nFs5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nC6, nRst, nC6, nRst, nC6, nRst, nC6, nRst
	dc.b	nC6, nRst, nC6, nRst, nC6, nRst, nC6, nRst, nC6
	smpsReturn

ssr_Call42:
	dc.b	nRst, $01, nC6, nRst, nC6, nRst, nC6, nRst, nC6, nRst, nC6, nRst
	dc.b	nC6, nRst, nC6, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst
	dc.b	nBb5, nRst, nBb5, nRst, nBb5, nRst, nBb5, nRst, nAb5, nRst, nAb5, nRst
	dc.b	nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst
	dc.b	nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst, nAb5, nRst
	dc.b	nAb5, nRst, nAb5, nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst
	dc.b	nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5, nRst, nFs5
	smpsReturn

ssr_Call43:
	dc.b	nRst, $01, nC6, nRst, nC6, nRst, nC6, nRst, nC6, nRst, nC6, nRst
	dc.b	nC6, nRst, nC6, nRst, nC4, $0F, nRst, $01, nBb3, $0B, nRst, $01
	dc.b	nC4, $22, nRst, $02, nC4, $0F, nRst, $01, nBb3, $0B, nRst, $01
	dc.b	nE4, $15
	smpsReturn

ssr_Call44:
	dc.b	smpsNoAttack, $0D, nRst, $02, nC4, $0F, nRst, $01, nBb3, $0B, nRst, $01
	dc.b	nC4, $22, nRst, $02, nC4, $0F, nRst, $01, nBb3, $0B, nRst, $01
	dc.b	nE4, $15
	smpsReturn

ssr_Call45:
	dc.b	smpsNoAttack, $0D, nRst, $73
	smpsReturn

ssr_Call35:
	smpsPSGvoice        fTone_06
	dc.b	nBb1, $01, nCs2, nEb2, nFs2, nAb2, nBb2, nCs3, nEb3, nFs3, nAb3, nBb3
	dc.b	nCs4, nEb4, nFs4, nAb4, nBb4, $04, nRst, $6D
	smpsReturn

ssr_Call36:
	dc.b	smpsNoAttack, nRst, $7F, smpsNoAttack, nRst, $01
	smpsReturn

ssr_Call37:
	dc.b	smpsNoAttack, nRst, $0F
	smpsPSGvoice        fTone_06
	dc.b	nF6, $01, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6
	dc.b	nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6
	dc.b	nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6
	dc.b	nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6
	dc.b	nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6
	dc.b	nRst, nF6, nRst, nF6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6
	dc.b	nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6
	dc.b	nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6
	dc.b	nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6
	dc.b	nRst, nCs6, nRst, nCs6, nRst, nCs6
	smpsReturn

ssr_Call38:
	dc.b	nRst, $01, nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst
	dc.b	nCs6, nRst, nCs6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nFs6, nRst, nFs6, nRst
	dc.b	nFs6, nRst, nFs6, nRst, nFs6, nRst, nFs6, nRst, nFs6, nRst, nFs6, nRst
	dc.b	nFs6, nRst, nFs6, nRst, nFs6, nRst, nFs6, nRst, nFs6, nRst, nFs6, nRst
	dc.b	nFs6, nRst, nFs6, nRst, nAb6, nRst, nAb6, nRst, nAb6, nRst, nAb6, nRst
	dc.b	nAb6, nRst, nAb6, nRst, nAb6, nRst, nAb6, nRst, nAb6
	smpsReturn

ssr_Call39:
	dc.b	nRst, $01, nAb6, nRst, nAb6, nRst, nAb6, nRst, nAb6, nRst, nAb6, nRst
	dc.b	nAb6, nRst, nAb6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst
	dc.b	nF6, nRst, nF6, nRst, nF6, nRst, nF6, nRst, nEb6, nRst, nEb6, nRst
	dc.b	nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst
	dc.b	nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst, nEb6, nRst
	dc.b	nEb6, nRst, nEb6, nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst
	dc.b	nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6, nRst, nCs6
	smpsReturn

ssr_Call3A:
	dc.b	nRst, $01, nAb6, nRst, nAb6, nRst, nAb6, nRst, nAb6, nRst, nAb6, nRst
	dc.b	nAb6, nRst, nAb6, nRst, nC6, $02, nC6, nC7, nC7, nC6, nC6, nC7
	dc.b	nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7
	dc.b	nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7
	dc.b	nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7
	dc.b	nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7
	dc.b	nC7, nC6, $01
	smpsReturn

ssr_Call3B:
	dc.b	smpsNoAttack, $01, nC6, $02, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6
	dc.b	nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6
	dc.b	nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6
	dc.b	nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6
	dc.b	nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6, nC7, nC7, nC6, nC6
	dc.b	nC7, nC7, nC6, nC6, nC7, nC7, nC6, $01
	smpsReturn

ssr_Call3C:
	dc.b	smpsNoAttack, $01, nC6, $02, nC7, nC7, nC6, nC6, nC7, nC7, nC5, $01
	dc.b	nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6
	dc.b	nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5
	dc.b	nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6
	dc.b	nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5
	dc.b	nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6
	dc.b	nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5
	dc.b	nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6
	dc.b	nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nEb5
	dc.b	nRst, nEb5, nRst, nEb6, nRst, nEb6, nRst, nEb5, nRst, nEb5, nRst, nEb6
	dc.b	nRst, nEb6, nRst, nEb5
	smpsReturn

ssr_Call3D:
	dc.b	nRst, $01, nEb5, nRst, nEb6, nRst, nEb6, nRst, nEb5, nRst, nEb5, nRst
	dc.b	nEb6, nRst, nEb6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst
	dc.b	nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst
	dc.b	nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst
	dc.b	nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst
	dc.b	nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst
	dc.b	nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst
	dc.b	nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst
	dc.b	nC5, nRst, nC5, nRst, nC6, nRst, nC6, nRst, nC5, nRst, nC5, nRst
	dc.b	nC6, nRst, nC6, nRst, nC5, nRst, $10
	smpsReturn

ssr_Call2E:
	smpsPSGvoice        fTone_02
	dc.b	nCs0, $01, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0
	dc.b	nCs0, nCs0, nCs0, nCs0, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst
	dc.b	nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst
	dc.b	nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst
	dc.b	nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst
	dc.b	nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst
	dc.b	nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst
	dc.b	nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst
	dc.b	nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst
	dc.b	nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst
	dc.b	nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0
	smpsReturn

ssr_Call2F:
	dc.b	nRst, $01
	smpsPSGvoice        fTone_02
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, $02, nCs0, nCs0, nCs0, $01
	smpsReturn

ssr_Call30:
	dc.b	smpsNoAttack, $01
	smpsPSGvoice        fTone_02
	dc.b	nCs0, $02, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0, $01, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0
	smpsReturn

ssr_Call31:
	dc.b	nRst, $01
	smpsPSGvoice        fTone_02
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0
	smpsReturn

ssr_Call32:
	dc.b	nRst, $01
	smpsPSGvoice        fTone_02
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	$05, nCs0, $02, $01
	smpsReturn

ssr_Call33:
	dc.b	smpsNoAttack, $01
	smpsPSGvoice        fTone_02
	dc.b	nCs0, $02, nCs0, nCs0, nCs0, nCs0, nCs0, nCs0, $0A, $01, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01
	dc.b	nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0
	dc.b	nRst, nCs0, $02, $01, nRst, nCs0
	smpsReturn

ssr_Call34:
	dc.b	nRst, $01
	smpsPSGvoice        fTone_02
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst
	dc.b	nCs0, nRst, nCs0, $02, $01, nRst, nCs0, nRst, nCs0, nRst, nCs0, $02
	dc.b	$01, nRst, nCs0, $02, nRst, $0F
	smpsReturn

ssr_Call00:
	dc.b	nRst, $0F, dSnare, $32, nRst, $02, dSnare, nRst, dSnare, $04, nRst, $02
	dc.b	dSnare, $01, nRst, dSnare, $04, nRst, dSnare, nRst, dSnare, $02, nRst, dSnare
	dc.b	nRst, dSnare, $04, nRst, $02, dSnare, dSnare, $04, nRst, dSnare, nRst, dSnare
	dc.b	$01
	smpsReturn

ssr_Call01:
	dc.b	nRst, $01, nRst, $02, dSnare, nRst, dSnare, $04, nRst, $02, dSnare, dSnare
	dc.b	$04, nRst, dSnare, nRst, dSnare, $02, nRst, dSnare, nRst, dSnare, $04, nRst
	dc.b	$02, dSnare, dSnare, $04, nRst, dSnare, nRst, dSnare, $02, nRst, dSnare, nRst
	dc.b	dSnare, $04, nRst, $02, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare
	dc.b	dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare
	dc.b	dSnare, dSnare, nRst, $06, dSnare, $01
	smpsReturn

ssr_Call02:
	dc.b	nRst, $01, nRst, $02, dSnare, nRst, dSnare, $04, nRst, $02, dSnare, dSnare
	dc.b	$20, $08, dSnare, dSnare, dSnare, $06, $02, $08, dSnare, dSnare, dSnare, dSnare
	dc.b	dSnare, dSnare, $01
	smpsReturn

ssr_Call03:
	dc.b	nRst, $07, dSnare, $04, dSnare, dSnare, $08, dSnare, dSnare, dSnare, dSnare, dSnare
	dc.b	dSnare, dSnare, $06, $02, $08, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, $01
	smpsReturn

ssr_Call04:
	dc.b	nRst, $07, dSnare, $06, $02, $20, $08, dSnare, dSnare, dSnare, $06, $02
	dc.b	$08, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, $01
	smpsReturn

ssr_Call05:
	dc.b	nRst, $07, dSnare, $04, dSnare, dSnare, $08, dSnare, dSnare, dSnare, dSnare, dSnare
	dc.b	dSnare, dSnare, $06, $02, $08, dSnare, dSnare, dSnare, dSnare, $11
	smpsReturn

ssr_Call06:
	dc.b	nRst, $0E, nRst, $01, dSnare, nRst, dSnare, nRst, dSnare, $02, $01, nRst
	dc.b	dSnare, $08, dSnare, dSnare, dSnare, dSnare, dSnare, dSnare, $06, $02, $08, dSnare
	dc.b	dSnare, dSnare, dSnare, dSnare, dSnare, $01
	smpsReturn

ssr_Call07:
	dc.b	nRst, $07, dSnare, $04, dSnare, dSnare, $08, dSnare, dSnare, dSnare, dSnare, dSnare
	dc.b	dSnare, dSnare, $06, $02, $08, dSnare, dSnare, dSnare, dSnare, $03, nRst, $0E
	smpsReturn

ssr_Voices:
;	Voice $00
;	$3C
;	$01, $00, $00, $00, 	$1F, $1F, $15, $1F, 	$11, $0D, $12, $05
;	$07, $04, $09, $02, 	$55, $3A, $25, $1A, 	$1A, $80, $07, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $00, $00, $00, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $15, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $05, $12, $0D, $11
	smpsVcDecayRate2    $02, $09, $04, $07
	smpsVcDecayLevel    $01, $02, $03, $05
	smpsVcReleaseRate   $0A, $05, $0A, $05
	smpsVcTotalLevel    $00, $07, $00, $1A

;	Voice $01
;	$3D
;	$01, $01, $01, $01, 	$94, $19, $19, $19, 	$0F, $0D, $0D, $0D
;	$07, $04, $04, $04, 	$25, $1A, $1A, $1A, 	$15, $80, $80, $80
	smpsVcAlgorithm     $05
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $01, $01, $01, $01
	smpsVcRateScale     $00, $00, $00, $02
	smpsVcAttackRate    $19, $19, $19, $14
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0D, $0D, $0D, $0F
	smpsVcDecayRate2    $04, $04, $04, $07
	smpsVcDecayLevel    $01, $01, $01, $02
	smpsVcReleaseRate   $0A, $0A, $0A, $05
	smpsVcTotalLevel    $00, $00, $00, $15

;	Voice $02
;	$03
;	$00, $D7, $33, $02, 	$5F, $9F, $5F, $1F, 	$13, $0F, $0A, $0A
;	$10, $0F, $02, $09, 	$35, $15, $25, $1A, 	$13, $16, $15, $80
	smpsVcAlgorithm     $03
	smpsVcFeedback      $00
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $03, $0D, $00
	smpsVcCoarseFreq    $02, $03, $07, $00
	smpsVcRateScale     $00, $01, $02, $01
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $0A, $0F, $13
	smpsVcDecayRate2    $09, $02, $0F, $10
	smpsVcDecayLevel    $01, $02, $01, $03
	smpsVcReleaseRate   $0A, $05, $05, $05
	smpsVcTotalLevel    $00, $15, $16, $13

;	Voice $03
;	$34
;	$70, $72, $31, $31, 	$1F, $1F, $1F, $1F, 	$10, $06, $06, $06
;	$01, $06, $06, $06, 	$35, $1A, $15, $1A, 	$10, $83, $18, $83
	smpsVcAlgorithm     $04
	smpsVcFeedback      $06
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $01, $01, $02, $00
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $06, $06, $06, $10
	smpsVcDecayRate2    $06, $06, $06, $01
	smpsVcDecayLevel    $01, $01, $01, $03
	smpsVcReleaseRate   $0A, $05, $0A, $05
	smpsVcTotalLevel    $03, $18, $03, $10

;	Voice $04
;	$3E
;	$77, $71, $32, $31, 	$1F, $1F, $1F, $1F, 	$0D, $06, $00, $00
;	$08, $06, $00, $00, 	$15, $0A, $0A, $0A, 	$1B, $80, $80, $80
	smpsVcAlgorithm     $06
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $01, $02, $01, $07
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $00, $06, $0D
	smpsVcDecayRate2    $00, $00, $06, $08
	smpsVcDecayLevel    $00, $00, $00, $01
	smpsVcReleaseRate   $0A, $0A, $0A, $05
	smpsVcTotalLevel    $00, $00, $00, $1B

;	Voice $05
;	$34
;	$33, $41, $7E, $74, 	$5B, $9F, $5F, $1F, 	$04, $07, $07, $08
;	$00, $00, $00, $00, 	$FF, $FF, $EF, $FF, 	$23, $80, $29, $87
	smpsVcAlgorithm     $04
	smpsVcFeedback      $06
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $07, $04, $03
	smpsVcCoarseFreq    $04, $0E, $01, $03
	smpsVcRateScale     $00, $01, $02, $01
	smpsVcAttackRate    $1F, $1F, $1F, $1B
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $08, $07, $07, $04
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $0F, $0E, $0F, $0F
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $07, $29, $00, $23

;	Voice $06
;	$3A
;	$01, $07, $31, $71, 	$8E, $8E, $8D, $53, 	$0E, $0E, $0E, $03
;	$00, $00, $00, $07, 	$1F, $FF, $1F, $0F, 	$18, $28, $27, $80
	smpsVcAlgorithm     $02
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $03, $00, $00
	smpsVcCoarseFreq    $01, $01, $07, $01
	smpsVcRateScale     $01, $02, $02, $02
	smpsVcAttackRate    $13, $0D, $0E, $0E
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $03, $0E, $0E, $0E
	smpsVcDecayRate2    $07, $00, $00, $00
	smpsVcDecayLevel    $00, $01, $0F, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $27, $28, $18

;	Voice $07
;	$3C
;	$32, $32, $71, $42, 	$1F, $18, $1F, $1E, 	$07, $1F, $07, $1F
;	$00, $00, $00, $00, 	$1F, $0F, $1F, $0F, 	$1E, $80, $0C, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $04, $07, $03, $03
	smpsVcCoarseFreq    $02, $01, $02, $02
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1E, $1F, $18, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $1F, $07, $1F, $07
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $00, $01, $00, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $0C, $00, $1E

;	Voice $08
;	$3C
;	$71, $72, $3F, $34, 	$8D, $52, $9F, $1F, 	$09, $00, $00, $0D
;	$00, $00, $00, $00, 	$23, $08, $02, $F7, 	$15, $80, $1D, $87
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $04, $0F, $02, $01
	smpsVcRateScale     $00, $02, $01, $02
	smpsVcAttackRate    $1F, $1F, $12, $0D
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0D, $00, $00, $09
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $0F, $00, $00, $02
	smpsVcReleaseRate   $07, $02, $08, $03
	smpsVcTotalLevel    $07, $1D, $00, $15

;	Voice $09
;	$3D
;	$01, $01, $00, $00, 	$8E, $52, $14, $4C, 	$08, $08, $0E, $03
;	$00, $00, $00, $00, 	$1F, $1F, $1F, $1F, 	$1B, $80, $80, $9B
	smpsVcAlgorithm     $05
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $00, $00, $01, $01
	smpsVcRateScale     $01, $00, $01, $02
	smpsVcAttackRate    $0C, $14, $12, $0E
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $03, $0E, $08, $08
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $01, $01, $01, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $1B, $00, $00, $1B

;	Voice $0A
;	$3A
;	$01, $01, $01, $02, 	$8D, $07, $07, $52, 	$09, $00, $00, $03
;	$01, $02, $02, $00, 	$52, $02, $02, $28, 	$18, $22, $18, $80
	smpsVcAlgorithm     $02
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $02, $01, $01, $01
	smpsVcRateScale     $01, $00, $00, $02
	smpsVcAttackRate    $12, $07, $07, $0D
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $03, $00, $00, $09
	smpsVcDecayRate2    $00, $02, $02, $01
	smpsVcDecayLevel    $02, $00, $00, $05
	smpsVcReleaseRate   $08, $02, $02, $02
	smpsVcTotalLevel    $00, $18, $22, $18

;	Voice $0B
;	$3C
;	$36, $31, $76, $71, 	$94, $9F, $96, $9F, 	$12, $00, $14, $0F
;	$04, $0A, $04, $0D, 	$2F, $0F, $4F, $2F, 	$33, $80, $1A, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $07, $03, $03
	smpsVcCoarseFreq    $01, $06, $01, $06
	smpsVcRateScale     $02, $02, $02, $02
	smpsVcAttackRate    $1F, $16, $1F, $14
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0F, $14, $00, $12
	smpsVcDecayRate2    $0D, $04, $0A, $04
	smpsVcDecayLevel    $02, $04, $00, $02
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $1A, $00, $33

;	Voice $0C
;	$34
;	$33, $41, $7E, $74, 	$5B, $9F, $5F, $1F, 	$04, $07, $07, $08
;	$00, $00, $00, $00, 	$FF, $FF, $EF, $FF, 	$23, $90, $29, $97
	smpsVcAlgorithm     $04
	smpsVcFeedback      $06
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $07, $04, $03
	smpsVcCoarseFreq    $04, $0E, $01, $03
	smpsVcRateScale     $00, $01, $02, $01
	smpsVcAttackRate    $1F, $1F, $1F, $1B
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $08, $07, $07, $04
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $0F, $0E, $0F, $0F
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $17, $29, $10, $23

;	Voice $0D
;	$38
;	$63, $31, $31, $31, 	$10, $13, $1A, $1B, 	$0E, $00, $00, $00
;	$00, $00, $00, $00, 	$3F, $0F, $0F, $0F, 	$1A, $19, $1A, $80
	smpsVcAlgorithm     $00
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $03, $06
	smpsVcCoarseFreq    $01, $01, $01, $03
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1B, $1A, $13, $10
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $00, $00, $0E
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $00, $00, $00, $03
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $1A, $19, $1A

;	Voice $0E
;	$3A
;	$31, $25, $73, $41, 	$5F, $1F, $1F, $9C, 	$08, $05, $04, $05
;	$03, $04, $02, $02, 	$2F, $2F, $1F, $2F, 	$29, $27, $1F, $80
	smpsVcAlgorithm     $02
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $04, $07, $02, $03
	smpsVcCoarseFreq    $01, $03, $05, $01
	smpsVcRateScale     $02, $00, $00, $01
	smpsVcAttackRate    $1C, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $05, $04, $05, $08
	smpsVcDecayRate2    $02, $02, $04, $03
	smpsVcDecayLevel    $02, $01, $02, $02
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $1F, $27, $29

;	Voice $0F
;	$04
;	$71, $41, $31, $31, 	$12, $12, $12, $12, 	$00, $00, $00, $00
;	$00, $00, $00, $00, 	$0F, $0F, $0F, $0F, 	$23, $80, $23, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $00
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $04, $07
	smpsVcCoarseFreq    $01, $01, $01, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $12, $12, $12, $12
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $00, $00, $00
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $00, $00, $00, $00
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $23, $00, $23

;	Voice $10
;	$14
;	$75, $72, $35, $32, 	$9F, $9F, $9F, $9F, 	$05, $05, $00, $0A
;	$05, $05, $07, $05, 	$2F, $FF, $0F, $2F, 	$1E, $80, $14, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $02
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $02, $05, $02, $05
	smpsVcRateScale     $02, $02, $02, $02
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $00, $05, $05
	smpsVcDecayRate2    $05, $07, $05, $05
	smpsVcDecayLevel    $02, $00, $0F, $02
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $14, $00, $1E

;	Voice $11
;	$3D
;	$01, $00, $01, $02, 	$12, $1F, $1F, $14, 	$07, $02, $02, $0A
;	$05, $05, $05, $05, 	$2F, $2F, $2F, $AF, 	$1C, $80, $82, $80
	smpsVcAlgorithm     $05
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $02, $01, $00, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $14, $1F, $1F, $12
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $02, $02, $07
	smpsVcDecayRate2    $05, $05, $05, $05
	smpsVcDecayLevel    $0A, $02, $02, $02
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $02, $00, $1C

;	Voice $12
;	$1C
;	$73, $72, $33, $32, 	$94, $99, $94, $99, 	$08, $0A, $08, $0A
;	$00, $05, $00, $05, 	$3F, $4F, $3F, $4F, 	$1E, $80, $19, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $03
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $02, $03, $02, $03
	smpsVcRateScale     $02, $02, $02, $02
	smpsVcAttackRate    $19, $14, $19, $14
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $08, $0A, $08
	smpsVcDecayRate2    $05, $00, $05, $00
	smpsVcDecayLevel    $04, $03, $04, $03
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $19, $00, $1E

;	Voice $13
;	$31
;	$33, $01, $00, $00, 	$9F, $1F, $1F, $1F, 	$0D, $0A, $0A, $0A
;	$0A, $07, $07, $07, 	$FF, $AF, $AF, $AF, 	$1E, $1E, $1E, $80
	smpsVcAlgorithm     $01
	smpsVcFeedback      $06
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $03
	smpsVcCoarseFreq    $00, $00, $01, $03
	smpsVcRateScale     $00, $00, $00, $02
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $0A, $0A, $0D
	smpsVcDecayRate2    $07, $07, $07, $0A
	smpsVcDecayLevel    $0A, $0A, $0A, $0F
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $1E, $1E, $1E

;	Voice $14
;	$3A
;	$70, $76, $30, $71, 	$1F, $95, $1F, $1F, 	$0E, $0F, $05, $0C
;	$07, $06, $06, $07, 	$2F, $4F, $1F, $5F, 	$21, $12, $28, $80
	smpsVcAlgorithm     $02
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $03, $07, $07
	smpsVcCoarseFreq    $01, $00, $06, $00
	smpsVcRateScale     $00, $00, $02, $00
	smpsVcAttackRate    $1F, $1F, $15, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0C, $05, $0F, $0E
	smpsVcDecayRate2    $07, $06, $06, $07
	smpsVcDecayLevel    $05, $01, $04, $02
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $28, $12, $21

;	Voice $15
;	$28
;	$71, $00, $30, $01, 	$1F, $1F, $1D, $1F, 	$13, $13, $06, $05
;	$03, $03, $02, $05, 	$4F, $4F, $2F, $3F, 	$0E, $14, $1E, $80
	smpsVcAlgorithm     $00
	smpsVcFeedback      $05
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $03, $00, $07
	smpsVcCoarseFreq    $01, $00, $00, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1D, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $05, $06, $13, $13
	smpsVcDecayRate2    $05, $02, $03, $03
	smpsVcDecayLevel    $03, $02, $04, $04
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $1E, $14, $0E

;	Voice $16
;	$3E
;	$38, $01, $7A, $34, 	$59, $D9, $5F, $9C, 	$0F, $04, $0F, $0A
;	$02, $02, $05, $05, 	$AF, $AF, $66, $66, 	$28, $80, $A3, $80
	smpsVcAlgorithm     $06
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $07, $00, $03
	smpsVcCoarseFreq    $04, $0A, $01, $08
	smpsVcRateScale     $02, $01, $03, $01
	smpsVcAttackRate    $1C, $1F, $19, $19
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $0F, $04, $0F
	smpsVcDecayRate2    $05, $05, $02, $02
	smpsVcDecayLevel    $06, $06, $0A, $0A
	smpsVcReleaseRate   $06, $06, $0F, $0F
	smpsVcTotalLevel    $00, $23, $00, $28

;	Voice $17
;	$39
;	$32, $31, $72, $71, 	$1F, $1F, $1F, $1F, 	$00, $00, $00, $00
;	$00, $00, $00, $00, 	$0F, $0F, $0F, $0F, 	$1B, $32, $28, $80
	smpsVcAlgorithm     $01
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $07, $03, $03
	smpsVcCoarseFreq    $01, $02, $01, $02
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $00, $00, $00
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $00, $00, $00, $00
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $28, $32, $1B

;	Voice $18
;	$07
;	$34, $74, $32, $71, 	$1F, $1F, $1F, $1F, 	$0A, $0A, $05, $03
;	$00, $00, $00, $00, 	$3F, $3F, $2F, $2F, 	$8A, $8A, $80, $80
	smpsVcAlgorithm     $07
	smpsVcFeedback      $00
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $03, $07, $03
	smpsVcCoarseFreq    $01, $02, $04, $04
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $03, $05, $0A, $0A
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $02, $02, $03, $03
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $00, $0A, $0A

;	Voice $19
;	$3A
;	$31, $37, $31, $31, 	$8D, $8D, $8E, $53, 	$0E, $0E, $0E, $03
;	$00, $00, $00, $00, 	$1F, $FF, $1F, $0F, 	$17, $28, $26, $80
	smpsVcAlgorithm     $02
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $03, $03
	smpsVcCoarseFreq    $01, $01, $07, $01
	smpsVcRateScale     $01, $02, $02, $02
	smpsVcAttackRate    $13, $0E, $0D, $0D
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $03, $0E, $0E, $0E
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $00, $01, $0F, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $26, $28, $17

;	Voice $1A
;	$3B
;	$3A, $31, $71, $74, 	$DF, $1F, $1F, $DF, 	$00, $0A, $0A, $05
;	$00, $05, $05, $03, 	$0F, $5F, $1F, $5F, 	$32, $1E, $0F, $80
	smpsVcAlgorithm     $03
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $07, $03, $03
	smpsVcCoarseFreq    $04, $01, $01, $0A
	smpsVcRateScale     $03, $00, $00, $03
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $05, $0A, $0A, $00
	smpsVcDecayRate2    $03, $05, $05, $00
	smpsVcDecayLevel    $05, $01, $05, $00
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $0F, $1E, $32

;	Voice $1B
;	$05
;	$04, $01, $02, $04, 	$8D, $1F, $15, $52, 	$06, $00, $00, $04
;	$02, $08, $00, $00, 	$1F, $0F, $0F, $2F, 	$16, $90, $84, $8C
	smpsVcAlgorithm     $05
	smpsVcFeedback      $00
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $04, $02, $01, $04
	smpsVcRateScale     $01, $00, $00, $02
	smpsVcAttackRate    $12, $15, $1F, $0D
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $04, $00, $00, $06
	smpsVcDecayRate2    $00, $00, $08, $02
	smpsVcDecayLevel    $02, $00, $00, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $0C, $04, $10, $16

;	Voice $1C
;	$2C
;	$71, $74, $32, $32, 	$1F, $12, $1F, $12, 	$00, $0A, $00, $0A
;	$00, $00, $00, $00, 	$0F, $1F, $0F, $1F, 	$16, $80, $17, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $05
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $02, $02, $04, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $12, $1F, $12, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $00, $0A, $00
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $01, $00, $01, $00
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $17, $00, $16

;	Voice $1D
;	$3A
;	$01, $07, $01, $01, 	$8E, $8E, $8D, $53, 	$0E, $0E, $0E, $03
;	$00, $00, $00, $07, 	$1F, $FF, $1F, $0F, 	$18, $28, $27, $8F
	smpsVcAlgorithm     $02
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $01, $01, $07, $01
	smpsVcRateScale     $01, $02, $02, $02
	smpsVcAttackRate    $13, $0D, $0E, $0E
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $03, $0E, $0E, $0E
	smpsVcDecayRate2    $07, $00, $00, $00
	smpsVcDecayLevel    $00, $01, $0F, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $0F, $27, $28, $18

;	Voice $1E
;	$36
;	$7A, $32, $51, $11, 	$1F, $1F, $59, $1C, 	$0A, $0D, $06, $0A
;	$07, $00, $02, $02, 	$AF, $5F, $5F, $5F, 	$1E, $8B, $81, $80
	smpsVcAlgorithm     $06
	smpsVcFeedback      $06
	smpsVcUnusedBits    $00
	smpsVcDetune        $01, $05, $03, $07
	smpsVcCoarseFreq    $01, $01, $02, $0A
	smpsVcRateScale     $00, $01, $00, $00
	smpsVcAttackRate    $1C, $19, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0A, $06, $0D, $0A
	smpsVcDecayRate2    $02, $02, $00, $07
	smpsVcDecayLevel    $05, $05, $05, $0A
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $01, $0B, $1E

;	Voice $1F
;	$3C
;	$71, $72, $3F, $34, 	$8D, $52, $9F, $1F, 	$09, $00, $00, $0D
;	$00, $00, $00, $00, 	$23, $08, $02, $F7, 	$15, $85, $1D, $8A
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $04, $0F, $02, $01
	smpsVcRateScale     $00, $02, $01, $02
	smpsVcAttackRate    $1F, $1F, $12, $0D
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0D, $00, $00, $09
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $0F, $00, $00, $02
	smpsVcReleaseRate   $07, $02, $08, $03
	smpsVcTotalLevel    $0A, $1D, $05, $15

;	Voice $20
;	$3E
;	$77, $71, $32, $31, 	$1F, $1F, $1F, $1F, 	$0D, $06, $00, $00
;	$08, $06, $00, $00, 	$15, $0A, $0A, $0A, 	$1B, $8F, $8F, $8F
	smpsVcAlgorithm     $06
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $03, $07, $07
	smpsVcCoarseFreq    $01, $02, $01, $07
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $00, $06, $0D
	smpsVcDecayRate2    $00, $00, $06, $08
	smpsVcDecayLevel    $00, $00, $00, $01
	smpsVcReleaseRate   $0A, $0A, $0A, $05
	smpsVcTotalLevel    $0F, $0F, $0F, $1B

;	Voice $21
;	$07
;	$34, $74, $32, $71, 	$1F, $1F, $1F, $1F, 	$0A, $0A, $05, $03
;	$00, $00, $00, $00, 	$3F, $3F, $2F, $2F, 	$8A, $8A, $8A, $8A
	smpsVcAlgorithm     $07
	smpsVcFeedback      $00
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $03, $07, $03
	smpsVcCoarseFreq    $01, $02, $04, $04
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $03, $05, $0A, $0A
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $02, $02, $03, $03
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $0A, $0A, $0A, $0A

