These are SMPS2ASM-converted Sonic 2 music files, which can theoretically be made to work with any SMPS driver.

You can find more of these, from different games, at:
http://forums.sonicretro.org/index.php?showtopic=26876

Note that these are not stock; I (Clownacy) have made modifications to these files, to fix several bug-causing mistakes within. The mistakes fixed include:
- Incorrect smpsVcTotalLevel values in '84 - OOZ.asm'
- Incorrect smpsVcTotalLevel values in '95 - SWEET SWEET SWEET.asm'
- Incorrect smpsVcTotalLevel values in '99 - Title Screen.asm'
