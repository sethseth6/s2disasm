heroes_Header:
	smpsHeaderStartSong 1
	smpsHeaderVoice     heroes_Voices
	smpsHeaderChan      $07, $03
	smpsHeaderTempo     $01, $09

	smpsHeaderDAC       heroes_DAC
	smpsHeaderFM        heroes_FM1,	$00, $0A
	smpsHeaderFM        heroes_FM2,	$00, $1B
	smpsHeaderFM        heroes_FM3,	$00, $1A
	smpsHeaderFM        heroes_FM4,	$00, $26
	smpsHeaderFM        heroes_FM5,	$00, $26
	smpsHeaderFM        heroes_FM6,	$00, $10
	smpsHeaderPSG       heroes_PSG1,	$00, $05, $00, $00
	smpsHeaderPSG       heroes_PSG2,	$00, $07, $00, $00
	smpsHeaderPSG       heroes_PSG3,	$00, $00, $00, $00

; FM1 Data
heroes_FM1:
	smpsSetvoice        $00

heroes_Jump01:
	dc.b	nD2, $7F, smpsNoAttack, $17, nC3, $0A, nD3, nD2, $78, nC3, $0A, nD3
	dc.b	nD2, nC3, nD3, nD2, $5A, nC3, $0A, nD3, nD2, $64
	smpsSetvoice        $05
	smpsAlterVol        $0C
	dc.b	nC3, $0A, nRst, $1E

heroes_Loop37:
	dc.b	nG3, $05, smpsNoAttack, nB3, smpsNoAttack, nD4, smpsNoAttack, nB3, smpsNoAttack
	smpsLoop            $00, $02, heroes_Loop37
	dc.b	nG3, nB3
	smpsSetvoice        $00
	smpsAlterVol        $F4
	dc.b	nA1, $0A, nA2, nB1, nB2, nC2, nC3, nE3, nD2, $14, nD3, $0A
	dc.b	nC3, nD2, nD3, nD2, nC3, nD2, nA1, nA2, nB1, nB2, nC2, nC3
	dc.b	nE3, nD2, $14, nD3, $0A, nC2, nC3, nB1, nB2, nG1, nG2, nA1
	dc.b	nA2, nB1, nB2, nC2, nC3, nE2, nD2, $14, nD3, $0A, nC2, nD2
	dc.b	$14, nC2, $0A, nB1, nG1, nA1, nA2, nB1, nB2, nC2, nC3, nE3
	dc.b	nD2, $14, nD3, $0A, nD2, nD3, nD3, nD2, nE2, nE3, nA1, nA2
	dc.b	nB1, nB2, nC2, nC3, nE3, nD2, $14, nD3, $0A, nC3, nD2, nD3
	dc.b	nD2, nC3, nD2, nA1, nA2, nB1, nB2, nC2, nC3, nE3, nD2, $14
	dc.b	nD3, $0A, nC2, nC3, nB1, nB2, nG1, nG2, nA1, nA2, nB1, nB2
	dc.b	nC2, nC3, nE2, nD2, $14, nD3, $0A, nC2, nD2, $14, nC2, $0A
	dc.b	nB1, nG1, nA1, nA2, nB1, nB2, nC2, nC3, nE3, nD2, $14, nD3
	dc.b	$0A, nD2, nD3, nD3, nD2, $14, nE2, $0A, nF2, $14, $04

heroes_Loop38:
	dc.b	nRst, $06, nF2, $04
	smpsLoop            $00, $04, heroes_Loop38
	dc.b	nRst, $06, nF2, $05, nRst, nE2, $0A, nB2, nE3, $05, nRst, nE3
	dc.b	$14, nB2, $0A, nE2, $14, nD2, $0A, nRst, nA2, nAb2, nG2, nD2
	dc.b	nG2, nC2, $46, nD2, $0A, nE2, nF2, nRst, nF2, $14, $0A, nC2
	dc.b	nF2, nF2, nE2, nE2, nB2, nE3, nE3, nB2, nE2, $14, nD2, nA2
	dc.b	$0A, nF2, nE2, nB2, nE3, nF2, $46, nRst, $14
	smpsSetvoice        $00
	dc.b	nD2, $7F, smpsNoAttack, $17, nC3, $0A, nD3, nD2, $78, nC3, $0A, nD3
	dc.b	nD2, nC3, nD3, nD2, $5A, nC3, $0A, nD3, nD2, $64
	smpsSetvoice        $0A
	dc.b	nRst, $5A, nD2, $50
	smpsSetvoice        $00
	dc.b	smpsNoAttack, $50, $0A, nD3, nD2, $3C, $0A, nD3, nD3, nD2, $32, $0A
	dc.b	nD3, nD2, $3C, $0A, nD3, nD2, $3C, $0A, nG3, nD2, $3C, $0A
	dc.b	nD3, nD2, $28, $0A
	smpsSetvoice        $0A
	dc.b	nD3, nRst, $7F, nRst, $21
	smpsSetvoice        $00

heroes_Loop3B:
	dc.b	nF2, $0A, nF3, nF2, nF3, nE2, nE3, nE2, nF2, $14, nF3, $0A
	dc.b	nF2, nF3, nE2, nE3, nE2, nE3, nF2, nF3, nF2, nF3, nE2, nE3
	dc.b	nE2, nE3, nA2, nA3, nB2, nB3, nC3, nC4, nF2, nF3, nD2, nD3
	dc.b	nD2, nD3, nG2, nG3, nG2, nG3, nE2, nE3, nE2, nE3, nA2, nA3
	dc.b	nA2, nA3

heroes_Loop39:
	dc.b	nD2, nD3
	smpsLoop            $00, $04, heroes_Loop39

heroes_Loop3A:
	dc.b	nG2, nG3
	smpsLoop            $00, $04, heroes_Loop3A
	smpsLoop            $01, $04, heroes_Loop3B
	smpsJump            heroes_Jump01

; FM2 Data
heroes_FM2:
	smpsSetvoice        $01
	smpsPan             panCenter, $00
	dc.b	nD4, $0A
	smpsAlterNote       $00

heroes_Loop2A:
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4
	smpsLoop            $00, $02, heroes_Loop2A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4, $5A, nC4, $04, nRst, $01, nC4, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop2B:
	dc.b	nD4, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop2B
	dc.b	nD4, $64
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop2C:
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4
	smpsLoop            $00, $02, heroes_Loop2C
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4, $5A, nC4, $04, nRst, $01, nC4, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop2D:
	dc.b	nD4, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop2D
	dc.b	nD4, nRst, $5A
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsModSet          $10, $01, $03, $08
	smpsAlterVol        $F0
	dc.b	nE4, $09, nRst, $01, nE4, $09, nRst, $01, nFs4, $0A, nRst, nG4
	dc.b	nRst, nB4, nA4, $46, nRst, $14, nE4, $09, nRst, $01, nE4, $09
	dc.b	nRst, $01, nFs4, $0A, nRst, nG4, nRst, nB4, nA4, $14, nG4, $05
	dc.b	nRst, nFs4, nRst, nE4, smpsNoAttack, $01
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nEb4, $01
	smpsAlterNote       $14
	dc.b	smpsNoAttack, nD4
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0A
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, nEb4, $01
	smpsAlterNote       $F5
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0C
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, nE4, $01
	smpsAlterNote       $F6
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $0F, nRst
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	nE4, $09, nRst, $01, nE4, $09, nRst, $01, nFs4, $0A, nRst, nG4
	dc.b	nRst, nB4, nA4, $46, nRst, $14, nE4, $09, nRst, $01, nE4, $09
	dc.b	nRst, $01, nFs4, $0A, nRst, nG4, nRst, nB4, nA4, $14, nG4, $05
	dc.b	nRst, nFs4, nRst, nE4, smpsNoAttack, $01
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nEb4, $01
	smpsAlterNote       $14
	dc.b	smpsNoAttack, nD4
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0A
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, nEb4, $01
	smpsAlterNote       $F5
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0C
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, nE4, $01
	smpsAlterNote       $F6
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $0F, nRst
	smpsSetvoice        $07
	smpsModOn
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	nE4, $09, nRst, $01, nE4, $09, nRst, $01, nFs4, $0A, nRst, nG4
	dc.b	nRst, nB4, nA4, $46, nRst, $14, nE4, $09, nRst, $01, nE4, $09
	dc.b	nRst, $01, nFs4, $0A, nRst, nG4, nRst, nB4, nA4, $14, nG4, $05
	dc.b	nRst, nFs4, nRst, nE4, smpsNoAttack, $01
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nEb4, $01
	smpsAlterNote       $14
	dc.b	smpsNoAttack, nD4
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0A
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, nEb4, $01
	smpsAlterNote       $F5
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0C
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, nE4, $01
	smpsAlterNote       $F6
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $0F, nRst
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	nE4, $09, nRst, $01, nE4, $09, nRst, $01, nFs4, $0A, nRst, nG4
	dc.b	nRst, nB4, nA4, $46, nRst, $14, nE4, $09, nRst, $01, nE4, $09
	dc.b	nRst, $01, nFs4, $0A, nRst, nG4, nRst, nB4, nA4, $14, nG4, $05
	dc.b	nRst, nFs4, nRst, nE4, smpsNoAttack, $01
	smpsAlterNote       $EA
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nEb4, $01
	smpsAlterNote       $14
	dc.b	smpsNoAttack, nD4
	smpsSetvoice        $09
	smpsAlterNote       $00
	smpsModOff
	smpsAlterVol        $11
	dc.b	nG4, $05, nA4, nB4, nC5, nA4, nB4, nC5, nD5, nE5, $32, nA4
	dc.b	$0A, nE5, nD5, $5A, nC5, $1E, nRst, $0A, nC5, nA4, nC5, nB4
	dc.b	$14, nG4, $0A, nE4, nG4, $28, nRst, $1E, nA4, $14, nC5, nA5
	dc.b	nG5, nB5, $0A, nC6, $14, $0A, nG5, nRst, nF5, nE5, nC5, nF5
	dc.b	nE5, nC5, nA4, nG4, $50, nRst, $0A
	smpsSetvoice        $01
	smpsPan             panCenter, $00
	smpsAlterVol        $FF

heroes_Loop2E:
	dc.b	nD4
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop2E
	dc.b	nD4, $5A, nC4, $04, nRst, $01, nC4, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop2F:
	dc.b	nD4, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop2F
	dc.b	nD4, $64
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop30:
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4
	smpsLoop            $00, $02, heroes_Loop30
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4, $5A, nC4, $04, nRst, $01, nC4, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop31:
	dc.b	nD4, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop31
	dc.b	nD4, $64
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop32:
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4
	smpsLoop            $00, $02, heroes_Loop32
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4, $5A, nC4, $04, nRst, $01, nC4, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop33:
	dc.b	nD4, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop33
	dc.b	nD4, $64
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop34:
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4
	smpsLoop            $00, $02, heroes_Loop34
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD4, $5A, nC4, $04, nRst, $01, nC4, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop35:
	dc.b	nD4, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, nD3
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop35
	dc.b	nD4, $64, nRst, $50
	smpsSetvoice        $09
	smpsAlterVol        $05
	dc.b	nD5, $05, nE5, nF5, nG5, nE5, nF5, nG5, nA5, nF5, nG5, nA5
	dc.b	nB5, nG5, nA5, nB5, nC6
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EB
	dc.b	nC4, $11, nRst, $03, nC4, $11, nRst, $03, nB3, $1E, nC4, $0A
	dc.b	nRst, nC4, nRst, nC4, nB3, $28, nC4, $11, nRst, $03, nC4, $11
	dc.b	nRst, $03, nB3, $07, nRst, $03, nB3, $14, nG3, $11, nRst, $03
	dc.b	nA3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nF4, nA4, nC5, nB4, $0A, nG4, nE4, nG4, $28, nRst, $0A, nF4
	dc.b	nE4, nC4, nF4, nE4, nC4, nA3, nG3, $50, nRst, $0A
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EF
	dc.b	nC4, $11, nRst, $03, nC4, $11, nRst, $03, nB3, $1E, nC4, $0A
	dc.b	nRst, nC4, nRst, nC4, nB3, $28, nC4, $11, nRst, $03, nC4, $11
	dc.b	nRst, $03, nB3, $07, nRst, $03, nB3, $14, nG3, $11, nRst, $03
	dc.b	nA3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nA4, nC5, nA5, nG5, nB5, $0A, nC6, $14, $0A, nG5, nF5, $14

heroes_Loop36:
	dc.b	nE5, $0A, nC5, nF5
	smpsLoop            $00, $04, heroes_Loop36
	dc.b	nE5, nC5, nD5
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EF
	dc.b	nC4, $11, nRst, $03, nC4, $11, nRst, $03, nB3, $1E, nC4, $0A
	dc.b	nRst, nC4, nRst, nC4, nB3, $28, nC4, $11, nRst, $03, nC4, $11
	dc.b	nRst, $03, nB3, $07, nRst, $03, nB3, $14, nG3, $11, nRst, $03
	dc.b	nA3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nF4, nA4, nC5, nB4, $0A, nG4, nE4, nG4, $28, nRst, $0A, nF4
	dc.b	nE4, nC4, nF4, nE4, nC4, nA3, nG3, $50, nRst, $0A
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EF
	dc.b	nC4, $11, nRst, $03, nC4, $11, nRst, $03, nB3, $1E, nC4, $0A
	dc.b	nRst, nC4, nRst, nC4, nB3, $28, nC4, $11, nRst, $03, nC4, $11
	dc.b	nRst, $03, nB3, $07, nRst, $03, nB3, $14, nG3, $11, nRst, $03
	dc.b	nA3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nA4, nC5, nA5, nG5, nB5, $0A, nC6, $14, $0A, nG5, nF5, $14
	dc.b	nE5, $0A, nC5, nF5, nE5, nC5, nA5, nG5, $50, nRst, $0A
	smpsAlterVol        $FF
	smpsJump            heroes_FM2

; FM3 Data
heroes_FM3:
	smpsSetvoice        $01
	smpsPan             panCenter, $00
	dc.b	nD3, $0A
	smpsAlterNote       $00

heroes_Loop1D:
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3
	smpsLoop            $00, $02, heroes_Loop1D
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3, $5A, nC3, $04, nRst, $01, nC3, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop1E:
	dc.b	nD3, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop1E
	dc.b	nD3, $64
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop1F:
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3
	smpsLoop            $00, $02, heroes_Loop1F
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3, $5A, nC3, $04, nRst, $01, nC3, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop20:
	dc.b	nD3, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop20
	dc.b	nD3, nRst, $5A
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsModSet          $10, $01, $03, $06
	smpsAlterVol        $F1
	dc.b	nC4, $09, nRst, $01, nC4, $09, nRst, $01, nD4, $0A, nRst, nE4
	dc.b	nRst, nG4, nFs4, $46, nRst, $14, nC4, $09, nRst, $01, nC4, $09
	dc.b	nRst, $01, nD4, $0A, nRst, nE4, nRst, nG4, nFs4, $14, nE4, $05
	dc.b	nRst, nD4, nRst, nC4, smpsNoAttack, $01
	smpsAlterNote       $EE
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nB3, $01
	smpsAlterNote       $20
	dc.b	smpsNoAttack, nBb3
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $10
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EF
	dc.b	smpsNoAttack, nB3, $01
	smpsAlterNote       $F7
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0A
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EE
	dc.b	smpsNoAttack, nC4, $01
	smpsAlterNote       $F8
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $0F, nRst
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	nC4, $09, nRst, $01, nC4, $09, nRst, $01, nD4, $0A, nRst, nE4
	dc.b	nRst, nG4, nFs4, $46, nRst, $14, nC4, $09, nRst, $01, nC4, $09
	dc.b	nRst, $01, nD4, $0A, nRst, nE4, nRst, nG4, nFs4, $14, nE4, $05
	dc.b	nRst, nD4, nRst, nC4, smpsNoAttack, $01
	smpsAlterNote       $EE
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nB3, $01
	smpsAlterNote       $20
	dc.b	smpsNoAttack, nBb3
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $10
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EF
	dc.b	smpsNoAttack, nB3, $01
	smpsAlterNote       $F7
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0A
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EE
	dc.b	smpsNoAttack, nC4, $01
	smpsAlterNote       $F8
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $0F, nRst
	smpsSetvoice        $07
	smpsModOn
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	nC4, $09, nRst, $01, nC4, $09, nRst, $01, nD4, $0A, nRst, nE4
	dc.b	nRst, nG4, nFs4, $46, nRst, $14, nC4, $09, nRst, $01, nC4, $09
	dc.b	nRst, $01, nD4, $0A, nRst, nE4, nRst, nG4, nFs4, $14, nE4, $05
	dc.b	nRst, nD4, nRst, nC4, smpsNoAttack, $01
	smpsAlterNote       $EE
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nB3, $01
	smpsAlterNote       $20
	dc.b	smpsNoAttack, nBb3
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $10
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EF
	dc.b	smpsNoAttack, nB3, $01
	smpsAlterNote       $F7
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $0A
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $EE
	dc.b	smpsNoAttack, nC4, $01
	smpsAlterNote       $F8
	dc.b	smpsNoAttack, $01
	smpsAlterNote       $00
	dc.b	smpsNoAttack, $0F, nRst
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	nC4, $09, nRst, $01, nC4, $09, nRst, $01, nD4, $0A, nRst, nE4
	dc.b	nRst, nG4, nFs4, $46, nRst, $14, nC4, $09, nRst, $01, nC4, $09
	dc.b	nRst, $01, nD4, $0A, nRst, nE4, nRst, nG4, nFs4, $14, nE4, $05
	dc.b	nRst, nD4, nRst, nC4, smpsNoAttack, $01
	smpsAlterNote       $EE
	dc.b	smpsNoAttack, $02
	smpsAlterNote       $FF
	dc.b	smpsNoAttack, nB3, $01
	smpsAlterNote       $20
	dc.b	smpsNoAttack, nBb3
	smpsSetvoice        $09
	smpsAlterNote       $00
	smpsModOff
	smpsAlterVol        $11
	dc.b	nG3, $05, nA3, nB3, nC4, nA3, nB3, nC4, nD4, nE4, $32, nA3
	dc.b	$0A, nE4, nD4, $5A, nC4, $1E, nRst, $0A, nC4, nA3, nC4, nB3
	dc.b	$14, nG3, $0A, nE3, nG3, $28, nRst, $1E, nA3, $14, nC4, nA4
	dc.b	nG4, nB4, $0A, nC5, $14, $0A, nG4, nRst, nF4, nE4, nC4, nF4
	dc.b	nE4, nC4, nA3, nG3, $50, nRst, $0A
	smpsSetvoice        $01
	smpsPan             panCenter, $00
	smpsAlterVol        $FE

heroes_Loop21:
	dc.b	nD3
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop21
	dc.b	nD3, $5A, nC3, $04, nRst, $01, nC3, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop22:
	dc.b	nD3, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop22
	dc.b	nD3, $64
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop23:
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3
	smpsLoop            $00, $02, heroes_Loop23
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3, $5A, nC3, $04, nRst, $01, nC3, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop24:
	dc.b	nD3, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop24
	dc.b	nD3, $64
	smpsSetvoice        $01
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop25:
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3
	smpsLoop            $00, $02, heroes_Loop25
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3, $5A, nC3, $04, nRst, $01, nC3, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop26:
	dc.b	nD3, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop26
	dc.b	nD3, $64
	smpsPan             panCenter, $00
	dc.b	$0A

heroes_Loop27:
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3
	smpsLoop            $00, $02, heroes_Loop27
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, nD3, $5A, nC3, $04, nRst, $01, nC3, $04, nRst, $01
	smpsPan             panCenter, $00

heroes_Loop28:
	dc.b	nD3, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, nD2
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack
	smpsLoop            $00, $03, heroes_Loop28
	dc.b	nD3, $64, nRst, $50
	smpsSetvoice        $09
	smpsAlterVol        $06
	dc.b	nD4, $05, nE4, nF4, nG4, nE4, nF4, nG4, nA4, nF4, nG4, nA4
	dc.b	nB4, nG4, nA4, nB4, nC5
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EB
	dc.b	nA3, $11, nRst, $03, nA3, $11, nRst, $03, nG3, $1E, nA3, $0A
	dc.b	nRst, nA3, nRst, nA3, nG3, $28, nA3, $11, nRst, $03, nA3, $11
	dc.b	nRst, $03, nG3, $07, nRst, $03, nG3, $14, nE3, $11, nRst, $03
	dc.b	nE3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nF3, nA3, nC4, nB3, $0A, nG3, nE3, nG3, $28, nRst, $0A, nF3
	dc.b	nE3, nC3, nF3, nE3, nC3, nA2, nG2, $50, nRst, $0A
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EF
	dc.b	nA3, $11, nRst, $03, nA3, $11, nRst, $03, nG3, $1E, nA3, $0A
	dc.b	nRst, nA3, nRst, nA3, nG3, $28, nA3, $11, nRst, $03, nA3, $11
	dc.b	nRst, $03, nG3, $07, nRst, $03, nG3, $14, nE3, $11, nRst, $03
	dc.b	nE3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nA3, nC4, nA4, nG4, nB4, $0A, nC5, $14, $0A, nG4, nF4, $14

heroes_Loop29:
	dc.b	nE4, $0A, nC4, nF4
	smpsLoop            $00, $04, heroes_Loop29
	dc.b	nE4, nC4, nD4
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EF
	dc.b	nA3, $11, nRst, $03, nA3, $11, nRst, $03, nG3, $1E, nA3, $0A
	dc.b	nRst, nA3, nRst, nA3, nG3, $28, nA3, $11, nRst, $03, nA3, $11
	dc.b	nRst, $03, nG3, $07, nRst, $03, nG3, $14, nE3, $11, nRst, $03
	dc.b	nE3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nF3, nA3, nC4, nB3, $0A, nG3, nE3, nG3, $28, nRst, $0A, nF3
	dc.b	nE3, nC3, nF3, nE3, nC3, nA2, nG2, $50, nRst, $0A
	smpsSetvoice        $07
	smpsPan             panCenter, $00
	smpsAlterVol        $EF
	dc.b	nA3, $11, nRst, $03, nA3, $11, nRst, $03, nG3, $1E, nA3, $0A
	dc.b	nRst, nA3, nRst, nA3, nG3, $28, nA3, $11, nRst, $03, nA3, $11
	dc.b	nRst, $03, nG3, $07, nRst, $03, nG3, $14, nE3, $11, nRst, $03
	dc.b	nE3, $46, nRst, $14
	smpsSetvoice        $09
	smpsAlterVol        $11
	dc.b	nA3, nC4, nA4, nG4, nB4, $0A, nC5, $14, $0A, nG4, nF4, $14
	dc.b	nE4, $0A, nC4, nF4, nE4, nC4, nA4, nG4, $50, nRst, $0A
	smpsAlterVol        $FE
	smpsJump            heroes_FM3

; FM4 Data
heroes_FM4:
	dc.b	nRst, $14
	smpsSetvoice        $03
	smpsPan             panRight, $00
	dc.b	nG2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $50, nRst, $46
	smpsAlterVol        $FC
	dc.b	nG2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $14
	smpsSetvoice        $08
	smpsPan             panLeft, $00
	smpsAlterVol        $F3

heroes_Loop15:
	dc.b	nC4, $0A, $0A, nD4, nRst, nE4, $1E, nD4, $50, nRst, $0A
	smpsSetvoice        $08
	smpsPan             panLeft, $00
	smpsLoop            $00, $07, heroes_Loop15
	dc.b	nC4, nC4, nD4, nRst, nE4, $1E, nD4, $50, nRst, $0A
	smpsPan             panRight, $00
	smpsAlterVol        $FA

heroes_Loop16:
	dc.b	nF4, $05, nA4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nE5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nF5, nE5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nA4
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	smpsLoop            $00, $02, heroes_Loop16

heroes_Loop17:
	dc.b	nE4, nG4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nB4, nD5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nE5, nD5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nB4, nG4
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	smpsLoop            $00, $02, heroes_Loop17
	dc.b	nD4, nF4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nA4, nC5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nD5, nC5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nA4, nAb4
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	nG4, nB4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nD5, nF5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nG5, nF5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nD5, nE5

heroes_Loop18:
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	nC4, nG4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nD5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nE5, nD5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nG4
	smpsLoop            $00, $02, heroes_Loop18

heroes_Loop19:
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	nF4, nA4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nE5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nF5, nE5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nA4
	smpsLoop            $00, $02, heroes_Loop19

heroes_Loop1A:
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	nE4, nG4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nB4, nD5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nG5, nD5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nB4, nG4
	smpsLoop            $00, $02, heroes_Loop1A
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	nF4, nA4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nF5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nA5, nF5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nA4
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	nB4, nD5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nE5, nG5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nB5, nG5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nE5, nD5
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	nF4, nA4
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nE5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nF5, nE5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nC5, nA4
	smpsPan             panRight, $00
	smpsAlterVol        $FC
	dc.b	$05, nC5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nE5, nC5
	smpsPan             panLeft, $00
	smpsAlterVol        $FC
	dc.b	nE5, nF5
	smpsPan             panCenter, $00
	smpsAlterVol        $04
	dc.b	nA5, nC6, $04
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $01, nRst, $14
	smpsSetvoice        $03
	smpsAlterVol        $0F
	smpsPan             panRight, $00
	dc.b	nG2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $50, nRst, $46, nG2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	nRst, $37
	smpsSetvoice        $05
	dc.b	nRst, $0F
	smpsAlterVol        $F7
	dc.b	nC3, $0A, nD3, nC3, nD3, nRst, $28, nA2, $0A, nD2, nRst, nD2

heroes_Loop1B:
	dc.b	nG3, $05, nB3, nD4, nB3, nG3, nB3, nD4, nB3, nG3, nB3, nRst
	dc.b	$0A, nD3, nRst
	smpsLoop            $00, $02, heroes_Loop1B
	dc.b	nG3, $05, nB3, nD4, nB3, nG3, nB3, nD4, nB3, nG3, nB3, nRst
	dc.b	$0A, nD3, nD3, nG3, $05, nB3, nD4, nB3, nG3, nB3, nD4, nB3
	dc.b	nG3, nB3, nRst, $0A
	smpsAlterVol        $01
	dc.b	nD3, nG3, nB3, $05, nD4, nB3, nG3, nD3, nB3, nG3, nD4, nG3
	dc.b	nD3, nB3, nG3, nD3, $0A, nG3, nB3, $05, nD4, nB3, nG3, nD3
	dc.b	nB3, nG3, nD4, nG3, nD3, nRst, $0A, nG3, nB3, $05, nD4, nB3
	dc.b	nD3, nG3, nB3, nD3, nG3, nB3, nG3, nD3, nD4, nD3, nG3, nB3
	dc.b	nD3, nG3, nD4, nB3, nG3, nD3, nG3, nB3, nD4, nG3, nB3, nD3
	dc.b	nG3, nRst, $7F, nRst, $21
	smpsSetvoice        $01
	smpsPan             panLeft, $00
	smpsAlterVol        $FD

heroes_Loop1C:
	dc.b	nF3, $28, nE3, $1E, nF3, $32, nE3, $28, nF3, nE3, nA3, $50
	dc.b	nD3, $28, nG3, nE3, nA3, nD3, $50, nG3
	smpsSetvoice        $01
	smpsPan             panLeft, $00
	smpsLoop            $00, $03, heroes_Loop1C
	dc.b	nF3, $28, nE3, $1E, nF3, $32, nE3, $28, nF3, nE3, nA3, $50
	dc.b	nD3, $28, nG3, nE3, nA3, nD3, $50, nG3
	smpsAlterVol        $0F
	smpsJump            heroes_FM4

; FM5 Data
heroes_FM5:
	dc.b	nRst, $14
	smpsSetvoice        $03
	smpsPan             panRight, $00
	dc.b	nE2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $50, nRst, $46
	smpsAlterVol        $FC
	dc.b	nE2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $14
	smpsSetvoice        $08
	smpsPan             panRight, $00
	smpsAlterVol        $F3

heroes_Loop13:
	dc.b	nE4, $0A, $0A, nFs4, nRst, nG4, $1E, nFs4, $50, nRst, $0A
	smpsSetvoice        $08
	smpsPan             panRight, $00
	smpsLoop            $00, $07, heroes_Loop13
	dc.b	nE4, nE4, nFs4, nRst, nG4, $1E, nFs4, $50, nRst, $0A
	smpsAlterVol        $FC
	dc.b	nC5, $50, nB4, nA4, nG4, nC5, nB4, nA4, $28, $28, $50, nRst
	dc.b	$14
	smpsSetvoice        $03
	smpsAlterVol        $11
	smpsPan             panRight, $00
	dc.b	nE2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $50, nRst, $46, nE2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $14
	smpsSetvoice        $03
	smpsPan             panLeft, $00
	dc.b	nRst
	smpsSetvoice        $03
	smpsPan             panRight, $00
	dc.b	nE2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $50, nRst, $46, nE2, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $3C
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $0A
	smpsPan             panRight, $00
	dc.b	smpsNoAttack, $1E
	smpsPan             panCenter, $00
	dc.b	smpsNoAttack, $28
	smpsPan             panLeft, $00
	dc.b	smpsNoAttack, $14, nRst, $7F, nRst, $21
	smpsSetvoice        $01
	smpsPan             panRight, $00
	smpsAlterVol        $F5

heroes_Loop14:
	dc.b	nF2, $28, nE2, $1E, nF2, $32, nE2, $28, nF2, nE2, nA2, $50
	dc.b	nD2, $28, nG2, nE2, nA2, nD2, $50, nG2
	smpsSetvoice        $01
	smpsPan             panRight, $00
	smpsLoop            $00, $03, heroes_Loop14
	dc.b	nF2, $28, nE2, $1E, nF2, $32, nE2, $28, nF2, nE2, nA2, $50
	dc.b	nD2, $28, nG2, nE2, nA2, nD2, $50, nG2
	smpsAlterVol        $0F
	smpsJump            heroes_FM5

; FM6 Data
heroes_FM6:
	smpsPan             panCenter, $00
	smpsSetvoice        $02

heroes_Jump00:
	smpsModSet          $00, $01, $C3, $FF
	dc.b	nE1

heroes_Loop00:
	dc.b	$14
	smpsSetvoice        $04
	smpsAlterVol        $08
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $F8
	smpsModOn
	smpsLoop            $00, $03, heroes_Loop00
	dc.b	$14
	smpsSetvoice        $04
	smpsAlterVol        $08
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $06
	smpsAlterVol        $20
	smpsModOn
	dc.b	nE4, $05
	smpsAlterVol        $F4
	dc.b	$05
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsAlterVol        $FE
	dc.b	$05
	smpsAlterVol        $FE
	dc.b	$05
	smpsAlterVol        $FD
	dc.b	$05
	smpsAlterVol        $FE

heroes_Loop01:
	dc.b	$03
	smpsAlterVol        $FF
	dc.b	$02
	smpsAlterVol        $FF
	smpsLoop            $00, $02, heroes_Loop01
	dc.b	$03, $02, $03, $02
	smpsAlterVol        $FF

heroes_Loop02:
	dc.b	$03, $02
	smpsLoop            $00, $04, heroes_Loop02
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $FE
	smpsModOn
	dc.b	nE1, $14
	smpsSetvoice        $04

heroes_Loop03:
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14
	smpsLoop            $00, $16, heroes_Loop03
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$05
	smpsSetvoice        $06
	smpsAlterVol        $21
	dc.b	nE4
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsSetvoice        $04
	smpsAlterVol        $EC
	dc.b	nE1
	smpsSetvoice        $06
	smpsAlterVol        $0E
	dc.b	nE4
	smpsAlterVol        $FE
	dc.b	$05
	smpsSetvoice        $04
	smpsAlterVol        $F4
	dc.b	nE1, nE1
	smpsSetvoice        $06
	smpsAlterVol        $05
	dc.b	nE4, $03
	smpsAlterVol        $FF

heroes_Loop04:
	dc.b	$02
	smpsAlterVol        $FF
	dc.b	$03, $02, $03
	smpsLoop            $00, $02, heroes_Loop04
	dc.b	$02, $03, $02, $03, $02
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $03

heroes_Loop05:
	smpsModOn
	dc.b	nE1, $14
	smpsSetvoice        $04
	smpsAlterVol        $08
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $F8
	smpsLoop            $00, $03, heroes_Loop05
	smpsModOn
	dc.b	$14
	smpsSetvoice        $04
	smpsAlterVol        $08
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $06
	smpsAlterVol        $20
	smpsModOn
	dc.b	nE4, $05
	smpsAlterVol        $F4
	dc.b	$05
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsAlterVol        $FE
	dc.b	$05
	smpsAlterVol        $FE
	dc.b	$05
	smpsAlterVol        $FD
	dc.b	$05
	smpsAlterVol        $FE

heroes_Loop06:
	dc.b	$03
	smpsAlterVol        $FF
	dc.b	$02
	smpsAlterVol        $FF
	smpsLoop            $00, $02, heroes_Loop06
	dc.b	$03, $02, $03, $02
	smpsAlterVol        $FF

heroes_Loop07:
	dc.b	$03, $02
	smpsLoop            $00, $04, heroes_Loop07
	smpsPan             panCenter, $00
	smpsModOn
	smpsSetvoice        $04
	smpsAlterVol        $FE
	dc.b	nE1

heroes_Loop08:
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	smpsLoop            $00, $08, heroes_Loop08
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $06
	smpsAlterVol        $2D
	smpsModOn
	dc.b	nE4, $05
	smpsAlterVol        $F4
	dc.b	$05
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsAlterVol        $FE
	dc.b	$05
	smpsAlterVol        $FE
	dc.b	$05
	smpsAlterVol        $FD
	dc.b	$05
	smpsAlterVol        $FE

heroes_Loop09:
	dc.b	$03
	smpsAlterVol        $FF
	dc.b	$02
	smpsAlterVol        $FF
	smpsLoop            $00, $02, heroes_Loop09
	dc.b	$03, $02, $03, $02
	smpsAlterVol        $FF

heroes_Loop0A:
	dc.b	$03, $02
	smpsLoop            $00, $04, heroes_Loop0A
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $FE
	smpsModOn
	dc.b	nE1, $14
	smpsSetvoice        $04

heroes_Loop0B:
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14
	smpsLoop            $00, $03, heroes_Loop0B
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsModOn
	dc.b	$14
	smpsSetvoice        $04
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsModOn
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$05
	smpsSetvoice        $06
	smpsAlterVol        $21
	dc.b	nE4
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsSetvoice        $04
	smpsAlterVol        $EC
	dc.b	nE1
	smpsSetvoice        $06
	smpsAlterVol        $0E
	dc.b	nE4
	smpsAlterVol        $FE
	dc.b	$05, $05
	smpsSetvoice        $04
	smpsAlterVol        $F4
	dc.b	nE1, $03
	smpsSetvoice        $06
	smpsAlterVol        $05
	dc.b	nE4, $02, $03
	smpsAlterVol        $FF
	dc.b	$02
	smpsAlterVol        $FF
	dc.b	$03, $02, $03, $02
	smpsSetvoice        $04
	smpsAlterVol        $FF
	smpsSetvoice        $06

heroes_Loop0C:
	dc.b	$03, $02
	smpsLoop            $00, $04, heroes_Loop0C
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $FE
	smpsModOn
	dc.b	nE1, $14
	smpsSetvoice        $04

heroes_Loop0D:
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14
	smpsLoop            $00, $03, heroes_Loop0D
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsModOn
	dc.b	$14
	smpsSetvoice        $04
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsModOn
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$05
	smpsSetvoice        $06
	smpsAlterVol        $21
	dc.b	nE4
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsSetvoice        $04
	smpsAlterVol        $EC
	dc.b	nE1
	smpsSetvoice        $06
	smpsAlterVol        $0E
	dc.b	nE4
	smpsAlterVol        $FE
	dc.b	$05, $05
	smpsSetvoice        $04
	smpsAlterVol        $F4
	dc.b	nE1, $03
	smpsSetvoice        $06
	smpsAlterVol        $05
	dc.b	nE4, $02, $03
	smpsAlterVol        $FF
	dc.b	$02
	smpsAlterVol        $FF
	dc.b	$03, $02, $03, $02
	smpsSetvoice        $04
	smpsAlterVol        $FF
	smpsSetvoice        $06

heroes_Loop0E:
	dc.b	$03, $02
	smpsLoop            $00, $04, heroes_Loop0E
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $FE
	smpsModOn
	dc.b	nE1, $14
	smpsSetvoice        $04

heroes_Loop0F:
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14
	smpsLoop            $00, $03, heroes_Loop0F
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsModOn
	dc.b	$14
	smpsSetvoice        $04
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsModOn
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$05
	smpsSetvoice        $06
	smpsAlterVol        $21
	dc.b	nE4
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsSetvoice        $04
	smpsAlterVol        $EC
	dc.b	nE1
	smpsSetvoice        $06
	smpsAlterVol        $0E
	dc.b	nE4
	smpsAlterVol        $FE
	dc.b	$05, $05
	smpsSetvoice        $04
	smpsAlterVol        $F4
	dc.b	nE1, $03
	smpsSetvoice        $06
	smpsAlterVol        $05
	dc.b	nE4, $02, $03
	smpsAlterVol        $FF
	dc.b	$02
	smpsAlterVol        $FF
	dc.b	$03, $02, $03, $02
	smpsSetvoice        $04
	smpsAlterVol        $FF
	smpsSetvoice        $06

heroes_Loop10:
	dc.b	$03, $02
	smpsLoop            $00, $04, heroes_Loop10
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsAlterVol        $FE
	smpsModOn
	dc.b	nE1, $14
	smpsSetvoice        $04

heroes_Loop11:
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14
	smpsLoop            $00, $03, heroes_Loop11
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsSetvoice        $02
	smpsModOn
	dc.b	$14
	smpsSetvoice        $04
	dc.b	$0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsModOn
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$14, $0F, $05, $14, $14
	smpsPan             panCenter, $00
	smpsModOn
	dc.b	$05
	smpsSetvoice        $06
	smpsAlterVol        $21
	dc.b	nE4
	smpsAlterVol        $F7
	dc.b	$05
	smpsAlterVol        $FC
	dc.b	$05
	smpsSetvoice        $04
	smpsAlterVol        $EC
	dc.b	nE1
	smpsSetvoice        $06
	smpsAlterVol        $0E
	dc.b	nE4
	smpsAlterVol        $FE
	dc.b	$05, $05
	smpsSetvoice        $04
	smpsAlterVol        $F4
	dc.b	nE1, $03
	smpsSetvoice        $06
	smpsAlterVol        $05
	dc.b	nE4, $02, $03
	smpsAlterVol        $FF
	dc.b	$02
	smpsAlterVol        $FF
	dc.b	$03, $02, $03, $02
	smpsSetvoice        $04
	smpsAlterVol        $FF
	smpsSetvoice        $06

heroes_Loop12:
	dc.b	$03, $02
	smpsLoop            $00, $04, heroes_Loop12
	smpsAlterVol        $03
	smpsJump            heroes_Jump00

; PSG1 Data
heroes_PSG1:
	dc.b	nRst

heroes_Loop4A:
	dc.b	$50
	smpsLoop            $00, $08, heroes_Loop4A

heroes_Loop4B:
	smpsPSGvoice        $00
	dc.b	nG1, $0A, $0A, nA1, nRst, nB1, $1E, nA1, $50, nRst, $0A
	smpsLoop            $00, $08, heroes_Loop4B
	dc.b	nA1, $50, nG1, nF1, nE1, nF1, nG1, nF1, $28, $23, $05, nC2
	dc.b	$50, nRst, $28
	smpsPSGAlterVol     $FF
	dc.b	nG2, $0A
	smpsPSGAlterVol     $02
	dc.b	nG1, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nG1, $04, nRst, $2E
	smpsPSGAlterVol     $02
	dc.b	nG1, $04, nRst, $01
	smpsPSGAlterVol     $FE
	dc.b	nG2, $05
	smpsPSGAlterVol     $02
	dc.b	nG1, $04, nRst, $0B, nG1, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nG1, $02, nRst, $03, nB1, $05
	smpsPSGAlterVol     $02
	dc.b	nD2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD2, $05
	smpsPSGAlterVol     $05
	dc.b	nG1, $02, nRst, $08
	smpsPSGAlterVol     $FB
	dc.b	nG2, $04, nRst, $24
	smpsPSGAlterVol     $05
	dc.b	nG3, $04, nRst, $06
	smpsPSGAlterVol     $FD
	dc.b	nG3, $04, nRst, $06
	smpsPSGAlterVol     $FE
	dc.b	nG3, $04, nRst, $1A, nG2, $04, nRst, $06, nE2, $05, nF2, nFs2
	dc.b	nG2, $04, nRst, $01, nG3, $04, nRst, $29
	smpsPSGAlterVol     $02
	dc.b	nG2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nG3, $04, nRst, $01, nG4, $05, nG3, nRst, nG3, $04, nRst, $06
	dc.b	nG3, $02, nRst, $08, nG3, $04, nRst, $06, nG3, $04, nRst, $06
	dc.b	nG3, $04, nRst, $01, nG2, $05, nG3, $04, nRst, $01, nG3, $04
	dc.b	nRst, $01
	smpsPSGAlterVol     $02
	dc.b	nG2, $04, nRst, $01
	smpsPSGAlterVol     $FE
	dc.b	nG3, $04, nRst, $29
	smpsPSGAlterVol     $05
	dc.b	nG1, $04, nRst, $01, nG1, $04, nRst, $01, nG1, $04, nRst, $06
	dc.b	nG1, $04, nRst, $01
	smpsPSGAlterVol     $FD
	dc.b	nG1, $04, nRst, $01
	smpsPSGAlterVol     $FE
	dc.b	nG1, $04, nRst, $01, nG2, $05
	smpsPSGAlterVol     $02
	dc.b	nG1, $04, nRst, $0B
	smpsPSGAlterVol     $03
	dc.b	nC2, $05, nG1, $04, nRst, $01
	smpsPSGAlterVol     $FD
	dc.b	nG1, $04, nRst, $01
	smpsPSGAlterVol     $FE
	dc.b	nG1, $04, nRst, $06
	smpsPSGAlterVol     $02
	dc.b	nG1, $04, nRst, $01
	smpsPSGAlterVol     $FE
	dc.b	nA1, $05, nG1, $02, nRst, $03

heroes_Loop4C:
	dc.b	nG2, $05
	smpsPSGAlterVol     $05
	dc.b	nA1
	smpsPSGAlterVol     $FD
	dc.b	$05
	smpsPSGAlterVol     $FE
	smpsLoop            $00, $02, heroes_Loop4C
	dc.b	nG2
	smpsPSGAlterVol     $02
	dc.b	nA1, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nG2, $05
	smpsPSGAlterVol     $05
	dc.b	nG1, $04, nRst, $01
	smpsPSGAlterVol     $FD
	dc.b	nG1, $04, nRst, $01
	smpsPSGAlterVol     $FE
	dc.b	nG2, $05, nRst
	smpsPSGAlterVol     $02
	dc.b	nG1, $02, nRst, $08, nG1, $02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $05
	smpsPSGAlterVol     $02
	dc.b	$02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FB
	dc.b	nB2, $05
	smpsPSGAlterVol     $05
	dc.b	$02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $05
	smpsPSGAlterVol     $02
	dc.b	$02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FB
	dc.b	nB2, $05
	smpsPSGAlterVol     $02
	dc.b	$02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FB
	dc.b	nB2, $02, nRst, $03

heroes_Loop4D:
	smpsPSGAlterVol     $05
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $05
	smpsLoop            $00, $02, heroes_Loop4D
	smpsPSGAlterVol     $05
	dc.b	$02, nRst, $03
	smpsPSGAlterVol     $FD

heroes_Loop4E:
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $07, nRst, $03
	smpsPSGAlterVol     $02
	smpsLoop            $00, $03, heroes_Loop4E
	dc.b	nB2, $02, nRst, $03, nB2, $02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $07, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $05
	smpsPSGAlterVol     $02
	dc.b	$02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FB
	dc.b	nB2, $05
	smpsPSGAlterVol     $02
	dc.b	$02, nRst, $03, nD3, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $05
	smpsPSGAlterVol     $05
	dc.b	$02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD3, $02, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $05, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nE3, $02, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FB
	dc.b	nD3, $07, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FB
	dc.b	nD3, $05
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $05
	smpsPSGAlterVol     $02
	dc.b	$02, nRst, $08, nB2, $02, nRst, $08

heroes_Loop4F:
	smpsPSGAlterVol     $FE
	dc.b	nD3, $07, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsLoop            $00, $02, heroes_Loop4F
	smpsPSGAlterVol     $FE
	dc.b	nD3, $05
	smpsPSGAlterVol     $02
	dc.b	nG2, nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2, $07, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD3, $05
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nB2

heroes_Loop50:
	dc.b	$07, nRst, $03
	smpsPSGAlterVol     $05
	dc.b	nB2, $05
	smpsPSGAlterVol     $FB
	smpsLoop            $00, $02, heroes_Loop50
	dc.b	nD3, $07, nRst, $03
	smpsPSGAlterVol     $05
	dc.b	nD3, $05
	smpsPSGAlterVol     $FB
	dc.b	nB2, $07, nRst, $03
	smpsPSGAlterVol     $05
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD

heroes_Loop51:
	dc.b	nB2, $02, nRst, $03
	smpsLoop            $00, $04, heroes_Loop51
	smpsPSGAlterVol     $FE
	dc.b	nD3, $05
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD3, $05, nG3
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03, nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD3, $07, nRst, $03, nD3, $05, nB2, $07, nRst, $03
	smpsPSGAlterVol     $02

heroes_Loop52:
	dc.b	nB2, $02, nRst, $03
	smpsLoop            $00, $04, heroes_Loop52
	smpsPSGAlterVol     $FE
	dc.b	nD3, $05
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03, nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD3, $05, nRst
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD3, $07, nRst, $08
	smpsPSGAlterVol     $02
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FE
	dc.b	nD3, $07, nRst, $03
	smpsPSGAlterVol     $02

heroes_Loop53:
	dc.b	nB2, $02, nRst, $03
	smpsLoop            $00, $03, heroes_Loop53

heroes_Loop54:
	smpsPSGAlterVol     $FB
	dc.b	nG0, $05, nA0, nB0, nC1, nA0, nB0, nC1, nD1, nB0, nC1, nD1
	dc.b	nE1, nC1, nD1, nE1, nF1, nD1, nE1, nF1, nG1, nE1, nF1, nG1
	dc.b	nA1, nF1, nG1, nA1, nB1, nG1, nA1, nB1, nC2
	smpsPSGvoice        $00
	smpsPSGAlterVol     $03
	dc.b	nE2, $11, nRst, $03, nE2, $11, nRst, $03, nD2, $1E, nE2, $0A
	dc.b	nRst, nE2, nRst, nE2, nD2, $28, nE2, $11, nRst, $03, nE2, $11
	dc.b	nRst, $03, nD2, $07, nRst, $03, nD2, $14, nB1, $0A
	smpsPSGAlterVol     $FE
	dc.b	nA1, $05, nB1, nC2, nD2, nB1, nC2, nD2, nE2, nC2, nD2, nE2
	dc.b	nF2, nD2, nE2, nF2, nG2, nRst, $14
	smpsPSGAlterVol     $04
	dc.b	nF2, nA2, nC3, nB2, $0A, nG2, nE2, nG2, $28, nRst, $0A, nF2
	dc.b	nE2, nC2, nF2, nE2, nC2, nA1, nG1
	smpsPSGAlterVol     $FC
	dc.b	nD1, $05, nE1, nF1, nG1, nE1, nF1, nG1, nA1, nF1, nG1, nA1
	dc.b	nB1, nG1, nA1, nB1, nC2
	smpsPSGvoice        $00
	smpsPSGAlterVol     $02
	dc.b	nE2, $11, nRst, $03, nE2, $11, nRst, $03, nD2, $1E, nE2, $0A
	dc.b	nRst, nE2, nRst, nE2, nD2, $28, nE2, $11, nRst, $03, nE2, $11
	dc.b	nRst, $03, nD2, $07, nRst, $03, nD2, $14, nB1, $0A
	smpsPSGAlterVol     $FE
	dc.b	nA1, $05, nB1, nC2, nD2, nB1, nC2, nD2, nE2, nC2, nD2, nE2
	dc.b	nF2, nD2, nE2, nF2, nG2, nRst, $14
	smpsPSGAlterVol     $04
	dc.b	nA2, nC3, nA3, nG3, nB3, $0A, nC4, $14, $0A, nG3, nF3
	smpsLoop            $00, $02, heroes_Loop54
	smpsPSGAlterVol     $FB
	dc.b	nG0, $05, nA0, nB0, nC1, nA0, nB0, nC1, nD1, nB0, nC1, nD1
	dc.b	nE1, nC1, nD1, nE1, nF1, nD1, nE1, nF1, nG1, nE1, nF1, nG1
	dc.b	nA1, nF1, nG1, nA1, nB1, nG1, nA1, nB1, nC2
	smpsPSGAlterVol     $04
	smpsJump            heroes_PSG1

; PSG2 Data
heroes_PSG2:
	dc.b	nRst

heroes_Loop3F:
	dc.b	$50
	smpsLoop            $00, $08, heroes_Loop3F

heroes_Loop40:
	smpsPSGvoice        $00
	dc.b	nC2, $0A, $0A, nD2, nRst, nE2, $1E, nD2, $50, nRst, $0A
	smpsLoop            $00, $03, heroes_Loop40
	smpsPSGvoice        $00
	dc.b	nC2, nC2, nD2, nRst, nE2, $1E, nD2, $50, nRst, $3C
	smpsPSGAlterVol     $FA
	dc.b	nG0, $0A, nC0, $04, nRst, $06, nAb0, $0A, nC0, nA0, nRst, nA0
	dc.b	$04, nRst, $10, nA0, $0A, nRst, nA0, nE1, nRst, $14, nA0, $04
	dc.b	nRst, $10, nA0, $0A, nRst, nA0, nC0, nA0, nRst, nA0, $04, nRst
	dc.b	$10, nA0, $0A, nRst, nA0, nE1, nD1, nRst, nA0, $04, nRst, $10
	dc.b	nG0, $0A, nC0, nAb0, nC0, nA0, nRst, nA0, $04, nRst, $10, nA0
	dc.b	$0A, nRst, nA0, nE1, nD1, nRst, nA0, $04, nRst, $10, nA0, $0A
	dc.b	nRst, nA0, nC0, nA0, nRst, nA0, $04, nRst, $10, nA0, $0A, nRst
	dc.b	nA0
	smpsPSGAlterVol     $04
	dc.b	nF1, $50, nE1, nD1, nC1, nA1, $14
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $FC
	dc.b	$14, nC2, nA2, nG2, nB2, $0A, nC3, $14, $0A, nG2, nRst, nF2
	dc.b	nE2, nC2, nF2, nE2, nC2, nA1, nG1, $55
	smpsPSGvoice        $00
	dc.b	smpsNoAttack, $05, nRst, $7F, nRst, $03
	smpsPSGAlterVol     $02
	dc.b	nE2, $02, nRst, $03, nE2, $05, nRst, $0A, nG2, $02, nRst, $03
	dc.b	nG2, $05

heroes_Loop41:
	dc.b	nRst, $0A, nB2, $02, nRst, $03, nB2, $05
	smpsLoop            $00, $02, heroes_Loop41
	dc.b	nRst, $0A, nE3, $02, nRst, $03, nE3, $05, nRst, $0A, nG3, $02
	dc.b	nRst, $03, nG3, $05, nRst, $0A, nB3, $02, nRst, $03, nB3, $05
	dc.b	nRst, $0A, nE4, $02, nRst, $03, nE4, $05, nRst, $0A, nB4, $02
	dc.b	nRst, $03, nB4, $05, nRst, $0A, nD5, $02, nRst, $03, nD5, $05
	dc.b	nRst, $0A, nB4, $02, nRst, $03, nB4, $05, nRst, nB4, $02, nRst
	dc.b	$08, nE4, $02, nRst, $08

heroes_Loop42:
	dc.b	nB3, $02, nRst, $03
	smpsLoop            $00, $03, heroes_Loop42
	dc.b	nG3, $05, nRst, nG3, $02, nRst, $03, nG3, $02, nRst, $03, nE3
	dc.b	$05, nRst, nB2, $02, nRst, $03, nB2, $02, nRst, $03, nB2, $05
	dc.b	$02, nRst, $08, nB2, $05, nRst

heroes_Loop43:
	dc.b	nG2, $02, nRst, $03
	smpsLoop            $00, $03, heroes_Loop43
	dc.b	nG2, $05, nRst, nG2, $02, nRst, $03, nG2, $02, nRst, $03, nE2
	dc.b	$05, nRst, nE2, $02, nRst, $03, nE2, $02, nRst, $03, nG2, $05
	dc.b	$02, nRst, $08, nG2, $05, nRst

heroes_Loop44:
	dc.b	nG2, $02, nRst, $03
	smpsLoop            $00, $03, heroes_Loop44
	dc.b	nB2, $05, nRst, nB2, $02, nRst, $03, nB2, $02, nRst, $03, nB2
	dc.b	$05, nRst, nG3, $02, nRst, $03, nG3, $02, nRst, $03, nG3, $05
	dc.b	nRst, nE4, $02, nRst, $03, nE4, $02, nRst, $03, nE4, $05, nRst
	dc.b	nB4, $02, nRst, $03, nB4, $02, nRst, $03, nB4, $05, nRst, nG5
	dc.b	$02, nRst, $03, nG5, $02, nRst, $7F, nRst, $7F, nRst, $4F
	smpsPSGAlterVol     $06
	dc.b	nB2, $02, nRst, $08, nB2, $02, nRst, $08

heroes_Loop45:
	smpsPSGAlterVol     $FD
	dc.b	nD3, $07, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsLoop            $00, $02, heroes_Loop45
	smpsPSGAlterVol     $FD
	dc.b	nD3, $05
	smpsPSGAlterVol     $03
	dc.b	nG2, nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nB2, $07, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nD3, $05
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nB2

heroes_Loop46:
	dc.b	$07, nRst, $03
	smpsPSGAlterVol     $06
	dc.b	nB2, $05
	smpsPSGAlterVol     $FA
	smpsLoop            $00, $02, heroes_Loop46
	dc.b	nD3, $07, nRst, $03
	smpsPSGAlterVol     $06
	dc.b	nD3, $05
	smpsPSGAlterVol     $FA
	dc.b	nB2, $07, nRst, $03
	smpsPSGAlterVol     $06
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD

heroes_Loop47:
	dc.b	nB2, $02, nRst, $03
	smpsLoop            $00, $04, heroes_Loop47
	smpsPSGAlterVol     $FD
	dc.b	nD3, $05
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nD3, $05, nG3
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03, nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nD3, $07, nRst, $03, nD3, $05, nB2, $07, nRst, $03
	smpsPSGAlterVol     $03

heroes_Loop48:
	dc.b	nB2, $02, nRst, $03
	smpsLoop            $00, $04, heroes_Loop48
	smpsPSGAlterVol     $FD
	dc.b	nD3, $05
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03, nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nD3, $05, nRst
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nD3, $07, nRst, $08
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03
	smpsPSGAlterVol     $FD
	dc.b	nD3, $07, nRst, $03
	smpsPSGAlterVol     $03
	dc.b	nB2, $02, nRst, $03, nB2, $02, nRst, $03, nB2, $02, nRst, $0D
	smpsPSGAlterVol     $FB
	dc.b	nG0, $05, nA0, nB0, nC1, nA0, nB0, nC1, nD1, nB0, nC1, nD1
	dc.b	nE1, nC1, nD1, nE1, nF1, nD1, nE1, nF1, nG1, nE1, nF1, nG1
	dc.b	nA1, nF1, nG1, nA1, nB1
	smpsPSGvoice        $00
	smpsPSGAlterVol     $FF
	dc.b	nA1, $11, nRst, $03, nA1, $11, nRst, $03, nG1, $1E, nA1, $0A
	dc.b	nRst, nA1, nRst, nA1, nG1, $28, nA1, $11, nRst, $03, nA1, $11
	dc.b	nRst, $03, nG1, $07, nRst, $03, nG1, $14, nE1, $11, nRst, $03
	dc.b	nE1, $0A
	smpsPSGAlterVol     $02
	dc.b	nA1, $05, nB1, nC2, nD2, nB1, nC2, nD2, nE2, nC2, nD2, nE2
	dc.b	nF2
	smpsPSGvoice        $00
	dc.b	nD2, nE2, nF2, nG2
	smpsPSGAlterVol     $FE
	dc.b	nF1, $14, nA1, nC2, nB1, $0A, nG1, nE1, nG1, $28, nRst, $0A
	dc.b	nF1, nE1, nC1, nF1, nE1, nC1, nA0, nG0, $1E
	smpsPSGAlterVol     $02
	dc.b	nD1, $05, nE1, nF1, nG1, nE1, nF1, nG1, nA1, nF1, nG1, nA1
	dc.b	nB1
	smpsPSGvoice        $00
	smpsPSGAlterVol     $FE
	dc.b	nA1, $11, nRst, $03, nA1, $11, nRst, $03, nG1, $1E, nA1, $0A
	dc.b	nRst, nA1, nRst, nA1, nG1, $28, nA1, $11, nRst, $03, nA1, $11
	dc.b	nRst, $03, nG1, $07, nRst, $03, nG1, $14, nE1, $11, nRst, $03
	dc.b	nE1, $0A
	smpsPSGAlterVol     $02
	dc.b	nA1, $05, nB1, nC2, nD2, nB1, nC2, nD2, nE2, nC2, nD2, nE2
	dc.b	nF2
	smpsPSGvoice        $00
	dc.b	nD2, nE2, nF2, nG2
	smpsPSGAlterVol     $FE
	dc.b	nA1, $14, nC2, nA2, nG2, nB2, $0A, nC3, $14, $0A, nG2, nF2
	dc.b	$14
	smpsPSGAlterVol     $FF

heroes_Loop49:
	dc.b	nE2, $0A, nC2, nF2
	smpsLoop            $00, $04, heroes_Loop49
	dc.b	nE2, nC2, nD2
	smpsPSGvoice        $00
	smpsPSGAlterVol     $01
	dc.b	nA1, $11, nRst, $03, nA1, $11, nRst, $03, nG1, $1E, nA1, $0A
	dc.b	nRst, nA1, nRst, nA1, nG1, $28, nA1, $11, nRst, $03, nA1, $11
	dc.b	nRst, $03, nG1, $07, nRst, $03, nG1, $14, nE1, $11, nRst, $03
	dc.b	nE1, $0A
	smpsPSGAlterVol     $02
	dc.b	nA1, $05, nB1, nC2, nD2, nB1, nC2, nD2, nE2, nC2, nD2, nE2
	dc.b	nF2
	smpsPSGvoice        $00
	dc.b	nD2, nE2, nF2, nG2
	smpsPSGAlterVol     $FE
	dc.b	nF1, $14, nA1, nC2, nB1, $0A, nG1, nE1, nG1, $28, nRst, $0A
	dc.b	nF1, nE1, nC1, nF1, nE1, nC1, nA0, nG0, $1E
	smpsPSGAlterVol     $02
	dc.b	nD1, $05, nE1, nF1, nG1, nE1, nF1, nG1, nA1, nF1, nG1, nA1
	dc.b	nB1
	smpsPSGvoice        $00
	smpsPSGAlterVol     $FE
	dc.b	nA1, $11, nRst, $03, nA1, $11, nRst, $03, nG1, $1E, nA1, $0A
	dc.b	nRst, nA1, nRst, nA1, nG1, $28, nA1, $11, nRst, $03, nA1, $11
	dc.b	nRst, $03, nG1, $07, nRst, $03, nG1, $14, nE1, $11, nRst, $03
	dc.b	nE1, $0A
	smpsPSGAlterVol     $02
	dc.b	nA1, $05, nB1, nC2, nD2, nB1, nC2, nD2, nE2, nC2, nD2, nE2
	dc.b	nF2
	smpsPSGvoice        $00
	dc.b	nD2, nE2, nF2, nG2
	smpsPSGAlterVol     $FE
	dc.b	nA1, $14, nC2, nA2, nG2, nB2, $0A, nC3, $14, $0A, nG2, nF2
	dc.b	$14, nE2, $0A
	smpsPSGAlterVol     $FF
	dc.b	nC2, nF2, nE2, nC2, nA2, nG2, $50
	smpsPSGAlterVol     $03
	dc.b	nA1, $05, nB1
	smpsPSGAlterVol     $02
	smpsJump            heroes_PSG2

; PSG3 Data
heroes_PSG3:
	smpsPSGform         $E7

heroes_Jump02:
	smpsPSGvoice        fTone_02

heroes_Loop3C:
	dc.b	nMaxPSG, $05, $05
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$0A
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $FE
	dc.b	nG5, $05
	smpsPSGvoice        fTone_02
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$05
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $02
	dc.b	nG5
	smpsPSGvoice        fTone_02
	smpsPSGAlterVol     $FC
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $05
	dc.b	nG5
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $FD
	dc.b	nMaxPSG, $0A
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $FE
	dc.b	nG5, $05
	smpsPSGvoice        fTone_02
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$05
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $02
	dc.b	nG5
	smpsPSGvoice        fTone_02
	smpsPSGAlterVol     $FC
	smpsLoop            $00, $27, heroes_Loop3C
	dc.b	nMaxPSG, nMaxPSG
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$0A
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $FE
	dc.b	nG5, $05
	smpsPSGvoice        fTone_02
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$05
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $02
	dc.b	nG5
	smpsPSGvoice        fTone_02
	smpsPSGAlterVol     $FC
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $05
	dc.b	nG5
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $FD
	dc.b	nMaxPSG, $0A
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $FE
	dc.b	nG5, $05
	smpsPSGvoice        fTone_02
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$05
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $02
	dc.b	nG5

heroes_Loop3D:
	dc.b	$5C, smpsNoAttack
	smpsLoop            $00, $06, heroes_Loop3D
	dc.b	$5D

heroes_Loop3E:
	smpsPSGvoice        fTone_02
	smpsPSGAlterVol     $FC
	dc.b	nMaxPSG, $05, $05
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$0A
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $FE
	dc.b	nG5, $05
	smpsPSGvoice        fTone_02
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$05
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $02
	dc.b	nG5
	smpsPSGvoice        fTone_02
	smpsPSGAlterVol     $FC
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $05
	dc.b	nG5
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $FD
	dc.b	nMaxPSG, $0A
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $FE
	dc.b	nG5, $05
	smpsPSGvoice        fTone_02
	dc.b	nMaxPSG
	smpsPSGvoice        fTone_01
	smpsPSGAlterVol     $02
	dc.b	$05
	smpsPSGvoice        fTone_04
	smpsPSGAlterVol     $02
	dc.b	nG5
	smpsLoop            $00, $22, heroes_Loop3E
	smpsPSGAlterVol     $FC
	smpsJump            heroes_Jump02

; DAC Data
heroes_DAC:
	smpsStop

heroes_Voices:
;	Voice $00
;	$03
;	$01, $00, $20, $00, 	$1F, $1F, $1F, $1F, 	$1F, $1F, $1F, $1F
;	$11, $00, $07, $00, 	$0F, $09, $0F, $07, 	$07, $2D, $19, $80
	smpsVcAlgorithm     $03
	smpsVcFeedback      $00
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $02, $00, $00
	smpsVcCoarseFreq    $00, $00, $00, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $1F, $1F, $1F, $1F
	smpsVcDecayRate2    $00, $07, $00, $11
	smpsVcDecayLevel    $00, $00, $00, $00
	smpsVcReleaseRate   $07, $0F, $09, $0F
	smpsVcTotalLevel    $00, $19, $2D, $07

;	Voice $01
;	$38
;	$32, $50, $50, $33, 	$1F, $1F, $1B, $17, 	$06, $07, $03, $00
;	$02, $00, $00, $00, 	$17, $16, $17, $03, 	$10, $1C, $18, $80
	smpsVcAlgorithm     $00
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $05, $05, $03
	smpsVcCoarseFreq    $03, $00, $00, $02
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $17, $1B, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $03, $07, $06
	smpsVcDecayRate2    $00, $00, $00, $02
	smpsVcDecayLevel    $00, $01, $01, $01
	smpsVcReleaseRate   $03, $07, $06, $07
	smpsVcTotalLevel    $00, $18, $1C, $10

;	Voice $02
;	$3C
;	$0F, $00, $01, $00, 	$1F, $14, $1F, $1F, 	$00, $0C, $16, $0F
;	$00, $1F, $17, $10, 	$F0, $FF, $60, $78, 	$00, $88, $00, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $00, $01, $00, $0F
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $14, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0F, $16, $0C, $00
	smpsVcDecayRate2    $10, $17, $1F, $00
	smpsVcDecayLevel    $07, $06, $0F, $0F
	smpsVcReleaseRate   $08, $00, $0F, $00
	smpsVcTotalLevel    $00, $00, $08, $00

;	Voice $03
;	$3E
;	$34, $34, $76, $78, 	$55, $0A, $4A, $0A, 	$00, $08, $00, $08
;	$01, $00, $00, $00, 	$05, $15, $05, $15, 	$28, $80, $8D, $80
	smpsVcAlgorithm     $06
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $07, $03, $03
	smpsVcCoarseFreq    $08, $06, $04, $04
	smpsVcRateScale     $00, $01, $00, $01
	smpsVcAttackRate    $0A, $0A, $0A, $15
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $08, $00, $08, $00
	smpsVcDecayRate2    $00, $00, $00, $01
	smpsVcDecayLevel    $01, $00, $01, $00
	smpsVcReleaseRate   $05, $05, $05, $05
	smpsVcTotalLevel    $00, $0D, $00, $28

;	Voice $04
;	$05
;	$00, $00, $00, $00, 	$1F, $1F, $1F, $1F, 	$12, $0C, $0C, $0C
;	$12, $18, $1F, $1F, 	$1F, $1F, $1F, $1F, 	$07, $80, $80, $80
	smpsVcAlgorithm     $05
	smpsVcFeedback      $00
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $00, $00, $00, $00
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1F, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $0C, $0C, $0C, $12
	smpsVcDecayRate2    $1F, $1F, $18, $12
	smpsVcDecayLevel    $01, $01, $01, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $00, $00, $07

;	Voice $05
;	$14
;	$00, $00, $00, $04, 	$1F, $0D, $1F, $14, 	$00, $00, $09, $04
;	$00, $00, $06, $08, 	$0F, $0F, $AF, $6F, 	$0F, $80, $00, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $02
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $04, $00, $00, $00
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $14, $1F, $0D, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $04, $09, $00, $00
	smpsVcDecayRate2    $08, $06, $00, $00
	smpsVcDecayLevel    $06, $0A, $00, $00
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $00, $00, $00, $0F

;	Voice $06
;	$3C
;	$0F, $00, $02, $00, 	$1F, $14, $1C, $1F, 	$00, $10, $19, $00
;	$00, $16, $00, $16, 	$F0, $78, $F0, $98, 	$00, $80, $00, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $00, $02, $00, $0F
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $1C, $14, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $19, $10, $00
	smpsVcDecayRate2    $16, $00, $16, $00
	smpsVcDecayLevel    $09, $0F, $07, $0F
	smpsVcReleaseRate   $08, $00, $08, $00
	smpsVcTotalLevel    $00, $00, $00, $00

;	Voice $07
;	$2C
;	$62, $02, $34, $14, 	$09, $1F, $09, $1F, 	$1F, $1F, $1F, $1F
;	$04, $00, $04, $00, 	$09, $09, $09, $09, 	$0F, $90, $0E, $90
	smpsVcAlgorithm     $04
	smpsVcFeedback      $05
	smpsVcUnusedBits    $00
	smpsVcDetune        $01, $03, $00, $06
	smpsVcCoarseFreq    $04, $04, $02, $02
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $09, $1F, $09
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $1F, $1F, $1F, $1F
	smpsVcDecayRate2    $00, $04, $00, $04
	smpsVcDecayLevel    $00, $00, $00, $00
	smpsVcReleaseRate   $09, $09, $09, $09
	smpsVcTotalLevel    $10, $0E, $10, $0F

;	Voice $08
;	$3C
;	$31, $52, $51, $30, 	$52, $53, $52, $53, 	$08, $00, $08, $00
;	$04, $00, $04, $00, 	$1F, $0F, $1F, $0F, 	$1A, $80, $13, $8B
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $03, $05, $05, $03
	smpsVcCoarseFreq    $00, $01, $02, $01
	smpsVcRateScale     $01, $01, $01, $01
	smpsVcAttackRate    $13, $12, $13, $12
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $00, $08, $00, $08
	smpsVcDecayRate2    $00, $04, $00, $04
	smpsVcDecayLevel    $00, $01, $00, $01
	smpsVcReleaseRate   $0F, $0F, $0F, $0F
	smpsVcTotalLevel    $0B, $13, $00, $1A

;	Voice $09
;	$27
;	$31, $71, $34, $74, 	$12, $11, $12, $0E, 	$04, $04, $04, $04
;	$00, $00, $00, $00, 	$1A, $1A, $17, $17, 	$80, $80, $80, $80
	smpsVcAlgorithm     $07
	smpsVcFeedback      $04
	smpsVcUnusedBits    $00
	smpsVcDetune        $07, $03, $07, $03
	smpsVcCoarseFreq    $04, $04, $01, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $0E, $12, $11, $12
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $04, $04, $04, $04
	smpsVcDecayRate2    $00, $00, $00, $00
	smpsVcDecayLevel    $01, $01, $01, $01
	smpsVcReleaseRate   $07, $07, $0A, $0A
	smpsVcTotalLevel    $00, $00, $00, $00

;	Voice $0A
;	$3C
;	$01, $00, $00, $00, 	$1F, $1F, $15, $1F, 	$11, $0D, $12, $05
;	$07, $04, $09, $02, 	$55, $3A, $25, $1A, 	$1A, $80, $07, $80
	smpsVcAlgorithm     $04
	smpsVcFeedback      $07
	smpsVcUnusedBits    $00
	smpsVcDetune        $00, $00, $00, $00
	smpsVcCoarseFreq    $00, $00, $00, $01
	smpsVcRateScale     $00, $00, $00, $00
	smpsVcAttackRate    $1F, $15, $1F, $1F
	smpsVcAmpMod        $00, $00, $00, $00
	smpsVcDecayRate1    $05, $12, $0D, $11
	smpsVcDecayRate2    $02, $09, $04, $07
	smpsVcDecayLevel    $01, $02, $03, $05
	smpsVcReleaseRate   $0A, $05, $0A, $05
	smpsVcTotalLevel    $00, $07, $00, $1A

